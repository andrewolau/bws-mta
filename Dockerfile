# usage: docker run bws-mta [-h] [-e END] [-t THREADS] [-w WORKER] [-c] [-d] [-m] start

# Run BWS MTA pipeline

# positional arguments:
#   start                 FW start date. Set to "latest" for latest FW.

# optional arguments:
#   -h, --help            show this help message and exit
#   -e END, --end END     FW end date
#   -t THREADS, --threads THREADS
#                         total number of threads
#   -w WORKER, --worker WORKER
#                         worker number
#   -c, --customer_journey_rerun
#                         whether to rerun customer journey creation. Default is
#                         False.
#   -d, --downstream_process_rerun
#                         whether to rerun downstream process. Default is False.
#   -m, --mta_rerun       whether to rerun MTA. Default is False.
#   -u, --update_final_table
#                         whether to update final MTA tables (Supers + Big W +
#                         BWS combined). Default is False.

# install docker
# curl -fsSL https://get.docker.com -o get-docker.sh
# sudo sh get-docker.sh

# build docker image
# sudo docker build -t bws-mta bws-mta

# save image and push to GCP bucket (service account isn't authenticating so I can't push to Art Re)
# sudo docker save -o /home/alau3/bws-mta/bws-mta-docker-image.tar bws-mta
# sudo gsutil cp bws-mta-docker-image.tar gs://wx-personal/AndrewLau/nash/bws-mta

# installing at new location
# sudo gsutil cp gs://wx-personal/AndrewLau/nash/bws-mta/bws-mta-docker-image.tar .
# sudo docker load -i bws-mta-docker-image.tar
# sudo docker run bws-mta [usage as above]

FROM python:3.7.3

COPY . /

RUN pip install -r requirements-pip.txt

ENTRYPOINT ["python", "/main.py"]

CMD [ "latest" ]
