
"""
"""
import os
import logging
import datetime

import google.auth
from google.cloud import bigquery
from google.cloud import bigquery_storage

import email_notifications

def query(fw,
                                    input_data="wx-bq-poc.loyalty_bi_analytics__prod_v3.safarievents_final_bws_",
                                    bqclient=None,
                                    bqstorageclient=None):
    """
    Overview
        Summarises BWS campaign attribution data for presentation.
        Write to `gcp-wow-rwds-ai-safari-prod.safari_campaign_attribution.bws_campaign_attribution_summary`
        https://console.cloud.google.com/bigquery?sq=564585695625:c825382b2091462c93920d80ba350c56
    Arguments
        fw - financial week, eg '2021-04-19'
        input_data - safari input data location, use safarievents_final_bws_ for FY20/21
    Returns
    """
    logging.info("running summarise_campaign_attribution for " + fw)
    fw_no_dash = fw[:4] + fw[5:7] + fw[-2:]

    query_string = """
    delete from `wx-bq-poc.personal.AL_bws_camp_check`
    where fw = '{fw}';

    insert into `wx-bq-poc.personal.AL_bws_camp_check` (
        with tmp as (
            SELECT
            campaign_code,
            campaign_ttl,
            case when activation_datetime is null then 0 else 1 end as activation_datetime_not_null,
            case when open_datetime is null then 0 else 1 end as open_datetime_not_null
            FROM
            `wx-bq-poc.loyalty_bi_analytics__prod_v3.safarievents_final_bws_{fw_no_dash}`
        )
        , tmp2 as (
            select
                campaign_code,
                campaign_ttl,
                sum(activation_datetime_not_null) as activation_datetime_not_null,
                sum(open_datetime_not_null) as open_datetime_not_null
            from tmp
            group by 1,2
        )
        select
            '{fw}' as fw,
            *,
            case
                when campaign_ttl = false and activation_datetime_not_null = 0 then 1
                when campaign_ttl = true and open_datetime_not_null = 0 then 1
                else 0
            end as error
        from tmp2
    );
    """.format(fw=fw, fw_no_dash=fw_no_dash, input_data=input_data)
    logging.debug(str(query_string))
    df_bq = (
    bqclient.query(query_string)
    .result()
    .to_dataframe(bqstorage_client=bqstorageclient)
    )

    logging.info("ran query for " + fw)

    return df_bq

if __name__ == "__main__":
    FW_START = '2020-06-29'
    FW_END = '2021-06-28'
    FW_MAX = 200
    INPUT_DATA = "wx-bq-poc.loyalty_bi_analytics__prod_v3.safarievents_final_bws_"
    BQ_PROJECT='gcp-wow-rwds-ai-safari-prod'

    ##################################################
    # LOGGING
    ##################################################
    # logging.basicConfig(level=logging.DEBUG)
    logFormatter = logging.Formatter("%(asctime)s [%(levelname)-7.7s]  %(message)s")
    rootLogger = logging.getLogger()
    rootLogger.setLevel(logging.INFO)  # need to set root logger to the lowest level

    # set user credentials to create BQ client object
    

    os.environ['GOOGLE_APPLICATION_CREDENTIALS']="adc.json" 
    os.environ['GOOGLE_CLOUD_PROJECT']=BQ_PROJECT
    logging.info("os.environ['GOOGLE_CLOUD_PROJECT'] = " + str(os.environ['GOOGLE_CLOUD_PROJECT']))

    # grab credentials from default login, use gcloud auth login
    credentials, your_project_id = google.auth.default(
        scopes=["https://www.googleapis.com/auth/cloud-platform"]
    )
    
    # Make clients.
    bqclient = bigquery.Client(credentials=credentials, project=BQ_PROJECT,)
    bqstorageclient = bigquery_storage.BigQueryReadClient(credentials=credentials)

    datetime_object = datetime.datetime.strptime(FW_START, '%Y-%m-%d')

    for weeks_to_add in range(FW_MAX):
        datetime_object_plus_weeks = datetime_object + datetime.timedelta(weeks=weeks_to_add)
        # break when reached last fw to parse
        if datetime_object_plus_weeks > datetime.datetime.strptime(FW_END, '%Y-%m-%d'):
            logging.info("breaking loop as FW_END reached")
            break
        fw = datetime_object_plus_weeks.strftime("%Y-%m-%d")
        fw_no_dash = fw[:4] + fw[5:7] + fw[-2:]
        
        query(fw, input_data=INPUT_DATA, bqclient=bqclient, bqstorageclient=bqstorageclient)

        logging.info("run complete for {fw_no_dash}`".format(fw_no_dash=fw_no_dash))
