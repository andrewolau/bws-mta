Please see the Confluence page for mor information:  
https://woolworthsdigital.atlassian.net/wiki/spaces/PAR/pages/25442746486/BWS+MTA  

# Installation and setup  
0. copy some kind of GCP account creds into os.environ['GOOGLE_APPLICATION_CREDENTIALS']="adc.json" 

Where can I find my GCP creds in a JSON on JupyterHub?
/home/jovyan/.config/gcloud/legacy_credentials/[YOUR_WOW_EMAIL]/adc.json"
`os.environ['GOOGLE_APPLICATION_CREDENTIALS']=f"/home/jovyan/.config/gcloud/legacy_credentials/{os.getenv('JUPYTERHUB_USER')}/adc.json"`

1. `bash 01_create_env_from_requirements.sh`
2. `source activate bws-mta`
3. `python main.py` usage below

## Docker image
install docker  
`curl -fsSL https://get.docker.com -o get-docker.sh`  
`sudo sh get-docker.sh`  

build docker image  
`sudo docker build -t bws-mta bws-mta`  

copy docker image (my service account couldn't authenticate with art re so I just saved it to a GCP bucket)  
`sudo gsutil cp gs://wx-personal/AndrewLau/nash/bws-mta/bws-mta-docker-image.tar .`  
`sudo docker load -i bws-mta-docker-image.tar`  
`sudo docker run bws-mta [usage below]`  

# Usage    
usage: python main.py [-h] [-e END] [-t THREADS] [-w WORKER] [-c] [-d] [-m] start  

Run BWS MTA pipeline  

**positional arguments:**  
  start                 FW start date. Set to "latest" for latest FW.  

**optional arguments:**  
  -h, --help            show this help message and exit  
  -e END, --end END     FW end date  
  -t THREADS, --threads THREADS  
                        total number of threads  
  -w WORKER, --worker WORKER  
                        worker number  
  -c, --customer_journey_rerun  
                        whether to rerun customer journey creation. Default is  
                        False.  
  -d, --downstream_process_rerun  
                        whether to rerun downstream process. Default is False.  
  -m, --mta_rerun       whether to rerun MTA. Default is False.  
  -u, --update_final_table  
                        whether to update final MTA tables (Supers + Big W +  
                        BWS combined). Default is False.  