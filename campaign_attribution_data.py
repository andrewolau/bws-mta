"""


"""
import os
import logging
import datetime

import google.auth
from google.cloud import bigquery
from google.cloud import bigquery_storage

import email_notifications
import utils

def generate_camp_event_data(fw,
                                    BQ_project="wx-bq-poc",
                                    input_data="wx-bq-poc.loyalty_bi_analytics__prod_v3.safarievents_final_",
                                    BQ_table="digital_attribution_modelling",
                                    bqclient=None,
                                    bqstorageclient=None,
                                    rerun=False):
    logging.info("running generate_camp_event_data for " + fw)
    fw_no_dash = fw[:4] + fw[5:7] + fw[-2:]
    # check if already ran
    if utils.check_if_BQ_table_exists("{BQ_project}.{BQ_table}.bws_camp_w_docket_events_{fw_no_dash}".format(BQ_project=BQ_project, BQ_table=BQ_table, fw_no_dash=fw_no_dash)):
        if rerun:
            logging.warning("rerunning generate_camp_event_data")
        else:
            logging.warning("skipping generate_camp_event_data")
            return
    query_string = """
    -- Overview:
    -- BWS
    -- Generate dataset for BWS campaign attribution. Will use activations,
    -- then email open, then email sent as touchpoints, depending on what
    -- is available for that campaign_code.
    -- Input:
    -- `{input_data}{fw_no_dash}`
    -- Output:
    -- `gcp-wow-rwds-ai-safari-prod.safari_campaign_attribution.bws_camp_w_docket_events_{fw_no_dash}`

    -- drop old table
    drop table if exists `gcp-wow-rwds-ai-safari-prod.safari_campaign_attribution.bws_camp_events_w_docket_{fw_no_dash}`;

    create or replace table `gcp-wow-rwds-ai-safari-prod.safari_campaign_attribution.bws_camp_no_docket_events_{fw_no_dash}` as (
        select * from `gcp-wow-rwds-ai-safari-prod.safari_campaign_attribution.bws_camp_events_{fw_no_dash}`
    );

    -- work out highest level of touchpoints available

    ----------------------------------------------------------------------
    -- WHAT TOUCHPOINTS TO USE FOR EACH CAMPAIGN CODE
    ----------------------------------------------------------------------
    create or replace table `gcp-wow-rwds-ai-safari-prod.safari_campaign_attribution.bws_camp_check_{fw_no_dash}` as (
        with tmp as (
            SELECT
            campaign_code,
            campaign_ttl,
            case when activation_datetime is null then 0 else 1 end as activation_datetime_not_null,
            case when open_datetime is null then 0 else 1 end as open_datetime_not_null,
            case when sent_datetime is null then 0 else 1 end as sent_datetime_not_null
            FROM
                `{input_data}{fw_no_dash}`
            where
                division_name = 'BWS' and
                cast(crn as STRING) != '0'
        )
        , tmp2 as (
            select
                campaign_code,
                campaign_ttl,
                sum(activation_datetime_not_null) as activation_datetime_not_null,
                sum(open_datetime_not_null) as open_datetime_not_null,
                sum(sent_datetime_not_null) as sent_datetime_not_null
            from tmp
            group by 1,2
        )
        select
            *,
            case
                when campaign_ttl = false and activation_datetime_not_null = 0 then 1
                when campaign_ttl = true and open_datetime_not_null = 0 then 1
                else 0
            end as TTL_mismatch,
            case
                -- docket campaign codes
                when
                    campaign_code in ('BCT-3700', 'BCT-3696', 'BCT-3695', 'BCT-3621', 'BCT-3562', 'BCT-3446', 'BCT-3380', 'BCT-3180',
                      'BCT-3154', 'BCT-3083', 'BCT-2837', 'BCT-2868', 'BCT-2823', 'BCT-2644', 'BCT-2582', 'BCT-2500', 'BCT-2414', 'BCT-3701',
                      -- manual exception from Rohit https://docs.google.com/spreadsheets/d/18MC8KF_tKINavMAc0I-IRX5BVcJUj7AJr8Op4TE8xZw/edit#gid=699541312
                      'BCV-3011'
                      )
                then "docket"
                when activation_datetime_not_null > 0 then "activation"
                when open_datetime_not_null > 0 then "open"
                when sent_datetime_not_null > 0 then "sent"            
                else "no activations, opens or sent available"
            end as event_used
        from tmp2
    );

    ----------------------------------------------------------------------
    -- SENT
    ----------------------------------------------------------------------
    create or replace table `gcp-wow-rwds-ai-safari-prod.safari_campaign_attribution.bws_camp_events_sent_{fw_no_dash}` as (
    with tmp as (
    select
        distinct cast(crn as STRING) as crn,
        campaign_code,
        campaign_start_date,
        campaign_end_date,
        fw_start_date,
        sent_datetime,
        -- take max(start of fw, open time)
        (case when cast(fw_start_date as timestamp) > sent_datetime then cast(fw_start_date as timestamp) else sent_datetime end) as ts
    from
        `{input_data}{fw_no_dash}`
    where
        fw_start_date = '{fw}' and
    --  BWS/Supers
        division_name = 'BWS' and
        cast(crn as STRING) != '0' and
        sent_datetime is not null and
        campaign_code in (
            select
                campaign_code
            from
                `gcp-wow-rwds-ai-safari-prod.safari_campaign_attribution.bws_camp_check_{fw_no_dash}`
            where
                event_used = "sent"
        )
    )
    select
    *,
    -- recalculate campaign week number, as Wilson's table dups for multiple week campaigns
    cast((case
            -- odd cases when activation time is before campaign start date. +1 to be inclusive of first date.
            when ceil((date_diff(cast(ts as date), campaign_start_date, DAY) + 1)/ 7) <= 0 then 1
            -- capping this to control odd behaviour, shouldn't really be campaigns lasting this long
            when ceil((date_diff(cast(ts as date), campaign_start_date, DAY) + 1)/ 7) >= 26 then 26
            else ceil((date_diff(cast(ts as date), campaign_start_date, DAY) + 1)/ 7)
        end    
    ) as INT64) as campaign_week_nbr
    from tmp
    )
    ;

    ----------------------------------------------------------------------
    -- OPENS
    ----------------------------------------------------------------------
    create or replace table `gcp-wow-rwds-ai-safari-prod.safari_campaign_attribution.bws_camp_events_open_{fw_no_dash}` as (
    with tmp as (
        select
            distinct cast(crn as STRING) as crn,
            campaign_code,
            campaign_start_date,
            campaign_end_date,
            fw_start_date,
            open_datetime,
            -- take max(start of fw, open time) as per Shen's request
            (case when cast(fw_start_date as timestamp) > open_datetime then cast(fw_start_date as timestamp) else open_datetime end) as ts
        from
            `{input_data}{fw_no_dash}`
        where
            fw_start_date = '{fw}' and
        --  BWS/Supers
            division_name = 'BWS' and
            cast(crn as STRING) != '0' and
            open_datetime is not null and
            campaign_code in (
                select
                    campaign_code
                from
                    `gcp-wow-rwds-ai-safari-prod.safari_campaign_attribution.bws_camp_check_{fw_no_dash}`
                where
                    event_used = "open"
            )
    )
    select
        *,
            -- recalculate campaign week number, as Wilson's table dups for multiple week campaigns
        cast((case
                -- odd cases when activation time is before campaign start date. +1 to be inclusive of first date.
                when ceil((date_diff(cast(ts as date), campaign_start_date, DAY) + 1)/ 7) <= 0 then 1
                -- capping this to control odd behaviour, shouldn't really be campaigns lasting this long
                when ceil((date_diff(cast(ts as date), campaign_start_date, DAY) + 1)/ 7) >= 26 then 26
                else ceil((date_diff(cast(ts as date), campaign_start_date, DAY) + 1)/ 7)
                end    
        ) as INT64) as campaign_week_nbr
    from tmp
    )
    ;
    ----------------------------------------------------------------------
    -- ACTIVATIONS
    ----------------------------------------------------------------------
    -- Confirmed with Wilson the timestamps in safarievents_final are SYDNEY time, despite being labelled as UTC

    create or replace table `gcp-wow-rwds-ai-safari-prod.safari_campaign_attribution.bws_camp_events_activation_{fw_no_dash}` as (
    with tmp as (
        select
        distinct cast(crn as STRING) as crn,
        campaign_code,
        campaign_start_date,
        campaign_end_date,  
        fw_start_date,
        activation_datetime,
        -- take max(start of fw, activation time)
        (case when cast(fw_start_date as timestamp) > activation_datetime then cast(fw_start_date as timestamp) else activation_datetime end) as ts
        from
            `{input_data}{fw_no_dash}`
        where
            fw_start_date = '{fw}' and
        --  BWS/Supers
            division_name = 'BWS' and
            cast(crn as STRING) != '0' and
            activation_datetime is not null and
            campaign_code in (
                select
                    campaign_code
                from
                    `gcp-wow-rwds-ai-safari-prod.safari_campaign_attribution.bws_camp_check_{fw_no_dash}`
                where
                    event_used = "activation"
            )
        )
    select
        *,
        -- recalculate campaign week number, as Wilson's table dups for multiple week campaigns
        cast((case
                -- odd cases when activation time is before campaign start date. +1 to be inclusive of first date.
                when ceil((date_diff(cast(ts as date), campaign_start_date, DAY) + 1)/ 7) <= 0 then 1
                -- capping this to control odd behaviour, shouldn't really be campaigns lasting this long
                when ceil((date_diff(cast(ts as date), campaign_start_date, DAY) + 1)/ 7) >= 26 then 26
                else ceil((date_diff(cast(ts as date), campaign_start_date, DAY) + 1)/ 7)
                end    
        ) as INT64) as campaign_week_nbr
    from tmp
    )
    ;

    ----------------------------------------------------------------------
    -- DOCKET
    ----------------------------------------------------------------------
    create or replace table `gcp-wow-rwds-ai-safari-prod.safari_campaign_attribution.bws_camp_events_docket_{fw_no_dash}` as (
    -- filter and clean campaign dates first
    with tmp0 as (
        select
            *,
            -- fixing start/end dates for nulls as well as being outside FW window
            case
                when campaign_start_date is null then fw_start_date
                -- if campaign_start date is BEFORE FW -> cup to start of FW
                when campaign_start_date < fw_start_date then fw_start_date
                else campaign_start_date
            end as campaign_start_date_fixed,
            case
                -- when redemption is null, set to end of week
                when redemption_datetime is null then date_add(fw_start_date, interval 6 day)
                -- when redemption is before end of fw -> set to start of fw
                when cast(redemption_datetime as date) < fw_start_date then fw_start_date
                -- when redemption is after start of fw -> set to end of fw
                when cast(redemption_datetime as date) > date_add(fw_start_date, interval 6 day) then date_add(fw_start_date, interval 6 day)
                else cast(redemption_datetime as date)
            end as redemption_date_fixed
        from `wx-bq-poc.loyalty_bi_analytics__prod_v3.safarievents_final_bws_{fw_no_dash}`
        where
            fw_start_date = '{fw}' and
            division_name = 'BWS' and
            cast(crn as STRING) != '0' and
            redemption_datetime is not null and
            -- docket campaign codes
            campaign_code in (
                'BCT-3700', 'BCT-3696', 'BCT-3695', 'BCT-3621', 'BCT-3562', 'BCT-3446', 'BCT-3380', 'BCT-3180',
                'BCT-3154', 'BCT-3083', 'BCT-2837', 'BCT-2868', 'BCT-2823', 'BCT-2644', 'BCT-2582', 'BCT-2500', 'BCT-2414', 'BCT-3701'
                -- mail campaign from Rohit
                -- https://docs.google.com/spreadsheets/d/18MC8KF_tKINavMAc0I-IRX5BVcJUj7AJr8Op4TE8xZw/edit#gid=699541312
                , 'BCV-3011')
    ),
    -- casting stuffs up the RNG, so have placed it in a separate CTE
    tmp as (
        select
            distinct cast(crn as STRING) as crn,
            campaign_code,
            campaign_start_date,
            campaign_start_date_fixed,
            campaign_end_date,
            redemption_datetime,
            redemption_date_fixed,
            fw_start_date,
            -- number of days between campaign_start_date_fixed and redemption
            date_diff(redemption_date_fixed, campaign_start_date_fixed, DAY) as date_diff,
            -- choose a random day between campaign_start_date_fixed and redemption day
            date_diff(redemption_date_fixed, campaign_start_date_fixed, DAY) * rand() as rand_date_add,
        from tmp0
    )
    -- casting stuffs up the RNG, so have placed it in a separate CTE
    , tmp1 as ( 
        select
            *,
            cast(rand_date_add as INT64) as rand_date_add_cast,
            cast(
                date_add(campaign_start_date_fixed,
                    interval
                        cast(rand_date_add as INT64)
                    day)
                as timestamp)
            as ts
        from tmp
    )
    select
        *,
        -- recalculate campaign week number, as Wilson's table dups for multiple week campaigns
        cast((case
                -- odd cases when activation time is before campaign start date. +1 to be inclusive of first date.
                when ceil((date_diff(cast(ts as date), campaign_start_date, DAY) + 1)/ 7) <= 0 then 1
                -- capping this to control odd behaviour, shouldn't really be campaigns lasting this long
                when ceil((date_diff(cast(ts as date), campaign_start_date, DAY) + 1)/ 7) >= 26 then 26
                else ceil((date_diff(cast(ts as date), campaign_start_date, DAY) + 1)/ 7)
            end    
        ) as INT64) as campaign_week_nbr,
        rand() as rand
    from tmp1
    );

    ----------------------------------------------------------------------
    -- UNION OF ALL TOUCHPOINTS
    ----------------------------------------------------------------------
    create or replace table `{BQ_project}.{BQ_table}.bws_camp_w_docket_events_{fw_no_dash}` as (
    with tmp as (
        -- sent touchpoints
        select 
            fw_start_date,
            crn,
            campaign_code,
            campaign_start_date,
            campaign_end_date,
            campaign_week_nbr,
            ts,
        --     concat(campaign_code, "_", campaign_start_date, "_wk", campaign_week_nbr) as event,
            concat(campaign_code, "_wk", campaign_week_nbr) as event,
            0 as TTL_flag,
            "sent" as campaign_event_type
        from
            `gcp-wow-rwds-ai-safari-prod.safari_campaign_attribution.bws_camp_events_sent_{fw_no_dash}`

        union all

        -- open touchpoints
        select 
            fw_start_date,
            crn,
            campaign_code,
            campaign_start_date,
            campaign_end_date,
            campaign_week_nbr,
            ts,
        --     concat(campaign_code, "_", campaign_start_date, "_wk", campaign_week_nbr) as event,
            concat(campaign_code, "_wk", campaign_week_nbr) as event,
            0 as TTL_flag,
            "open" as campaign_event_type
        from
            `gcp-wow-rwds-ai-safari-prod.safari_campaign_attribution.bws_camp_events_open_{fw_no_dash}`

        union all

        -- activation touchpoints
        select 
            fw_start_date,
            crn,
            campaign_code,
            campaign_start_date,
            campaign_end_date,
            campaign_week_nbr,
            ts,
        --     concat(campaign_code, "_", campaign_start_date, "_wk", campaign_week_nbr) as event,
            concat(campaign_code, "_wk", campaign_week_nbr) as event,
            0 as TTL_flag,
            "activation" as campaign_event_type
        from
            `gcp-wow-rwds-ai-safari-prod.safari_campaign_attribution.bws_camp_events_activation_{fw_no_dash}`

        union all

        -- docket touchpoints
        select 
            fw_start_date,
            crn,
            campaign_code,
            campaign_start_date,
            campaign_end_date,
            campaign_week_nbr,
            ts,
        --     concat(campaign_code, "_", campaign_start_date, "_wk", campaign_week_nbr) as event,
            concat(campaign_code, "_wk", campaign_week_nbr) as event,
            0 as TTL_flag,
            "docket" as campaign_event_type
        from
            `gcp-wow-rwds-ai-safari-prod.safari_campaign_attribution.bws_camp_events_docket_{fw_no_dash}`
    )
    select
        a.*,
        case when b.crn is null then 0 else 1 end as redemption_flag
    from tmp as a
    -- join on redemption flag
    left join (
        select
            distinct
            cast(crn as STRING) as crn,
            campaign_code,
            campaign_start_date,
            fw_start_date,
        from 
            `{input_data}{fw_no_dash}`
        where
            fw_start_date = '{fw}' and
            division_name = 'BWS' and
            cast(crn as STRING) != '0' and
            redemption_flag_campaign_week is true
    ) as b
        on a.crn = b.crn
        and a.campaign_code = b.campaign_code
        and a.campaign_start_date = b.campaign_start_date
        and a.fw_start_date = b.fw_start_date
    );

    ----------------------------------------------------------------------
    -- CHECKS
    ----------------------------------------------------------------------
    -- check TTL flag only has 1 value per campaign code
    ASSERT
    (
        with tmp as (
            select distinct campaign_code, TTL_flag from `gcp-wow-rwds-ai-safari-prod.safari_campaign_attribution.bws_camp_w_docket_events_{fw_no_dash}`
        ), tmp2 as (
            select
                campaign_code,
                count(*) as count
            from tmp group by 1
        )
        select count(*) from tmp2 where count !=1
    ) = 0
    AS 'ERROR: inconsistent TTL flags';

    -- check that each date is mapped to only 1 campaign week number
    ASSERT
    (
        (with tmp as (
            select 
                campaign_code,
                campaign_start_date,
                campaign_week_nbr,
                cast(ts as date) as ts_date,
                count(*) as count
            from 
                `gcp-wow-rwds-ai-safari-prod.safari_campaign_attribution.bws_camp_w_docket_events_{fw_no_dash}`
            group by 1,2,3,4
            order by 1,2,3,4
        )
        ,tmp2 as (
            select
                campaign_code,
                campaign_start_date,
                campaign_week_nbr,
                ts_date,
                count(*) as count
            from tmp
            group by 1,2,3,4

        )
        select count(*) from tmp2 where count != 1) = 0
    ) AS 'ERROR: inconsistent campaign week number';

    -- check consistent FW
    ASSERT
    (
    WITH
    tmp AS (
    SELECT
        DISTINCT fw_start_date,
        campaign_code
    FROM
        `gcp-wow-rwds-ai-safari-prod.safari_campaign_attribution.bws_camp_w_docket_events_{fw_no_dash}`
    WHERE
        fw_start_date != '{fw}'
    ORDER BY
        1, 2)
    SELECT
        COUNT(*)
    FROM
        tmp
    ) = 0
    AS 'ERROR: invalid FWs';

    -- check all campaigns are there
    ASSERT
    (
        select
            count(distinct campaign_code)
        from
            `gcp-wow-rwds-ai-safari-prod.safari_campaign_attribution.bws_camp_check_{fw_no_dash}`
        where event_used != "no activations, opens or sent available"
    ) = 
    (
        select
            count(distinct campaign_code)
        from
            `gcp-wow-rwds-ai-safari-prod.safari_campaign_attribution.bws_camp_w_docket_events_{fw_no_dash}`
    )

    AS 'ERROR: number of distinct campaign codes in output does not match input';

    """.format(fw=fw, fw_no_dash=fw_no_dash, BQ_project=BQ_project, BQ_table=BQ_table, input_data=input_data)
    logging.debug(str(query_string))
    df_bq = (
    bqclient.query(query_string)
    .result()
    .to_dataframe(bqstorage_client=bqstorageclient)
    )

    logging.info("ran generate_camp_event_data for " + fw)
    logging.info("`{BQ_project}.{BQ_table}.bws_camp_w_docket_events_{fw_no_dash}`".format(BQ_project=BQ_project, BQ_table=BQ_table, fw_no_dash=fw_no_dash))

    return df_bq


if __name__ == "__main__":
    ##################################################
    # LOGGING
    ##################################################
    # logging.basicConfig(level=logging.DEBUG)
    logFormatter = logging.Formatter("%(asctime)s | %(filename)s | function: %(funcName)s | line: %(lineno)d : %(levelname)s - %(message)s")
    rootLogger = logging.getLogger()
    rootLogger.setLevel(logging.INFO)  # need to set root logger to the lowest level
    
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.INFO)
    consoleHandler.setFormatter(logFormatter)
    rootLogger.addHandler(consoleHandler)

    def generate_campaign_attribution_data(fw_start,
                                            fw_end,
                                            BQ_project="wx-bq-poc",
                                            BQ_table="digital_attribution_modelling",
                                            input_data="wx-bq-poc.loyalty_bi_analytics__prod_v3.safarievents_final_",
                                            rerun=False,
                                            FW_MAX=100,
                                            bqclient=None,
                                            bqstorageclient=None):
        # set user credentials to create BQ client object
        os.environ['GOOGLE_APPLICATION_CREDENTIALS']="adc.json" 
        os.environ['GOOGLE_CLOUD_PROJECT']=BQ_project
        logging.info("os.environ['GOOGLE_CLOUD_PROJECT'] = " + str(os.environ['GOOGLE_CLOUD_PROJECT']))
        logging.info("using input_data = " + str(input_data))

        # grab credentials from default login, use gcloud auth login
        credentials, your_project_id = google.auth.default(
            scopes=["https://www.googleapis.com/auth/cloud-platform"]
        )
        
        # Make clients.
        bqclient = bigquery.Client(credentials=credentials, project=BQ_project,)
        bqstorageclient = bigquery_storage.BigQueryReadClient(credentials=credentials)

        datetime_object = datetime.datetime.strptime(fw_start, '%Y-%m-%d')

        for weeks_to_add in range(FW_MAX):        
            datetime_object_plus_weeks = datetime_object + datetime.timedelta(weeks=weeks_to_add)
            # break when reached last fw to parse
            if datetime_object_plus_weeks > datetime.datetime.strptime(fw_end, '%Y-%m-%d'):
                logging.info("breaking loop as fw_end reached")
                break
            fw = datetime_object_plus_weeks.strftime("%Y-%m-%d")
            fw_no_dash = fw[:4] + fw[5:7] + fw[-2:]

            generate_camp_event_data(fw, input_data=input_data, rerun=rerun, BQ_project=BQ_project, BQ_table=BQ_table, bqclient=bqclient, bqstorageclient=bqstorageclient)
    
            logging.info("`gcp-wow-rwds-ai-safari-prod.safari_campaign_attribution.bws_camp_w_docket_events_{fw_no_dash}`".format(BQ_project=BQ_project, BQ_table=BQ_table, fw_no_dash=fw_no_dash))

    BQ_PROJECT_MAPPING = {"gcp-wow-rwds-ai-safari-prod":"safari_campaign_attribution"
        # , "wx-bq-poc":"digital_attribution_modelling"
        }

    for BQ_PROJECT in BQ_PROJECT_MAPPING:
        BQ_TABLE = BQ_PROJECT_MAPPING[BQ_PROJECT]

        logging.info("BQ_PROJECT = " + str(BQ_PROJECT))
        logging.info("BQ_TABLE = " + str(BQ_TABLE))

        # all weeks, use BWS Safari events data
        generate_campaign_attribution_data("2020-01-06", "2021-08-30", 
                                          input_data="wx-bq-poc.loyalty_bi_analytics__prod_v3.safarievents_final_bws_",
                                          BQ_project=BQ_PROJECT, BQ_table=BQ_TABLE, rerun=True)

        email_notifications.send_email_notification(email_recipient='alau3@woolworths.com.au', subject="BWS MTA CAMPAIGN DATA DONE", message_body="", fpath_image=None)

    
