"""
usage: main.py [-h] [-e END] [-t THREADS] [-w WORKER] [-c] [-d] [-m] start

Run BWS MTA pipeline

positional arguments:
  start                 FW start date. Set to "latest" for latest FW.

optional arguments:
  -h, --help            show this help message and exit
  -e END, --end END     FW end date
  -t THREADS, --threads THREADS
                        total number of threads
  -w WORKER, --worker WORKER
                        worker number
  -c, --customer_journey_rerun
                        whether to rerun customer journey creation. Default is
                        False.
  -d, --downstream_process_rerun
                        whether to rerun downstream process. Default is False.
  -m, --mta_rerun       whether to rerun MTA. Default is False.
  -u, --update_final_table
                        whether to update final MTA tables (Supers + Big W +
                        BWS combined). Default is False.
"""
import channel_attribution
import customer_journeys
import downstream_process
import dashboard
import final_tables
import email_notifications

import logging
import datetime
import sys
import os
import time
import argparse

import google.auth
from google.cloud import bigquery
from google.cloud import bigquery_storage

##################################################
# LOGGING
##################################################
LOG_TO_FILE = True  # whether to dump debug and all errors to file

logFormatter = logging.Formatter("%(asctime)s [%(levelname)-7.7s]  %(message)s")
rootLogger = logging.getLogger()
if LOG_TO_FILE:
    rootLogger.setLevel(logging.DEBUG)  # need to set root logger to the lowest level
else:
    rootLogger.setLevel(logging.INFO)  # need to set root logger to the lowest level

now = datetime.datetime.now() + datetime.timedelta(hours=10)  # convert to Sydney time
now = now.strftime("%d-%m-%Y_%H%M")

if not os.path.exists('logs'):
    os.makedirs('logs')

fileHandler = logging.FileHandler("{0}/{1}.log".format("logs", now), 'w+')
fileHandler.setLevel(logging.DEBUG)
fileHandler.setFormatter(logFormatter)
rootLogger.addHandler(fileHandler)

consoleHandler = logging.StreamHandler()
consoleHandler.setLevel(logging.INFO)
consoleHandler.setFormatter(logFormatter)
rootLogger.addHandler(consoleHandler)

if LOG_TO_FILE:
    # catch all exceptions in the log
    def my_handler(type, value, tb):
        rootLogger.exception("Uncaught exception: {0}".format(str(value)))

    # Install exception handler
    sys.excepthook = my_handler

##################################################
# SETTING UP GCP CREDENTIALS AND BQ CLIENTS
##################################################
# set user credentials to create BQ client object
os.environ['GOOGLE_APPLICATION_CREDENTIALS']="adc.json" 
os.environ['GOOGLE_CLOUD_PROJECT']="wx-bq-poc" 

# grab creentials from default login, use gcloud auth login
credentials, your_project_id = google.auth.default(
    scopes=["https://www.googleapis.com/auth/cloud-platform"]
)

# make clients.
bqclient = bigquery.Client(credentials=credentials, project='wx-bq-poc',)
bqstorageclient = bigquery_storage.BigQueryReadClient(credentials=credentials)   

##################################################
# MTA PIPELINE
##################################################
EXCEPTIONS = '2020-03-23'
def run_pipeline(fw_start, fw_end=None, fw_max=100, thread=0, num_threads=1):
    # if no end provided (eg run for just one week)
    if fw_end is None:
        fw_end = fw_start
    datetime_object = datetime.datetime.strptime(fw_start, '%Y-%m-%d')
    counter = 0

    for weeks_to_add in range(fw_max):
        logging.debug("num_threads = " + str(num_threads))
        logging.debug("thread = " + str(thread))
        logging.debug("counter = " + str(counter))
        logging.debug("counter % num_threads = " + str(counter % num_threads))
        # use modulus to run multiple threads
        if (counter % num_threads) == thread:
            # 5 second delay between threads so BQ doesn't choke up with too many queries
            time.sleep(10 * thread)
            datetime_object_plus_weeks = datetime_object + datetime.timedelta(weeks=weeks_to_add)
            # if datetime.datetime.strptime(fw_end, '%Y-%m-%d') == datetime.datetime.strptime(EXCEPTIONS, '%Y-%m-%d'):
            #     logging.info("breaking loop due to exception")
            #     break

            # safari events input data. Older periods are saved somewhere different
            if datetime_object_plus_weeks <= datetime.datetime.strptime("2021-08-30", '%Y-%m-%d'):
                input_data="wx-bq-poc.loyalty_bi_analytics__prod_v3.safarievents_final_bws_"
                logging.info('fw <= 2021-08-30, input_data="wx-bq-poc.loyalty_bi_analytics__prod_v3.safarievents_final_bws_"')
            else:
                input_data="wx-bq-poc.loyalty_bi_analytics__prod_v3.safarievents_final_"
                logging.info('fw > 2021-08-30, input_data="wx-bq-poc.loyalty_bi_analytics__prod_v3.safarievents_final_"')

            # break when reached last fw to parse
            if datetime_object_plus_weeks > datetime.datetime.strptime(fw_end, '%Y-%m-%d') or counter > fw_max:
                logging.info("breaking loop as fw_end reached")
                break
            fw = datetime_object_plus_weeks.strftime("%Y-%m-%d")        

            try:
                logging.info("running entire MTA pipeline for " + str(fw) + " on thread number " + str(thread) + " out of " + str(num_threads) + " threads")
                # generate customer journeys
                customer_journeys.create_customer_journeys(fw=fw, input_data=input_data, bqclient=bqclient, bqstorageclient=bqstorageclient, use_dummy_data=False, use_dummy_date_extended=False, rerun=args.customer_journey_rerun)
                # run MTA
                channel_attribution.run_mta(fw=fw, bqclient=bqclient, bqstorageclient=bqstorageclient, rerun=args.mta_rerun)
                # run downstream process
                downstream_process.run_downstream_process(fw=fw, input_data=input_data, bqclient=bqclient, bqstorageclient=bqstorageclient, rerun=args.downstream_process_rerun)
                # upload to dashboard
                dashboard.upload_dashboard_data(fw=fw, bqclient=bqclient, bqstorageclient=bqstorageclient)                
                # update final MTA tables (Supers + Big W + BWS)
                if args.update_final_table:
                    logging.info("updating final MTA tables")
                    final_tables.update_final_tables(fw=fw, bqclient=bqclient, bqstorageclient=bqstorageclient)

                email_notifications.send_email_notification(email_recipient='alau3@woolworths.com.au', subject="BWS MTA SUCCESS on thread = " + str(thread) + " fw = " + str(fw), message_body="", fpath_image=None)
                
            except Exception as e:
                logging.error("An exception occurred: " + str(e))
                email_notifications.send_email_notification(email_recipient='alau3@woolworths.com.au', subject="BWS MTA ERROR on thread = " + str(thread) + " fw = " + str(fw), message_body=str(e), fpath_image=None)
        counter += 1

##################################################
# COMMAND LINE INPUT
##################################################
my_parser = argparse.ArgumentParser(description='Run BWS MTA pipeline')

my_parser.add_argument('start',
                       metavar='start',
                       type=str,
                       help='FW start date. Set to "latest" for latest FW.')

my_parser.add_argument('-e',
                       '--end',
                       action='store',
                       help='FW end date')

my_parser.add_argument('-t',
                       '--threads',
                       action='store',
                       help='total number of threads')

my_parser.add_argument('-w',
                       '--worker',
                       action='store',
                       help='worker number')

my_parser.add_argument('-c',
                       '--customer_journey_rerun',
                       action='store_true',
                       help='whether to rerun customer journey creation. Default is False.')

my_parser.add_argument('-d',
                       '--downstream_process_rerun',
                       action='store_true',
                       help='whether to rerun downstream process. Default is False.')

my_parser.add_argument('-m',
                       '--mta_rerun',
                       action='store_true',
                       help='whether to rerun MTA. Default is False.')

my_parser.add_argument('-u',
                       '--update_final_table',
                       action='store_true',
                       help='whether to update final MTA tables (Supers + Big W + BWS combined). Default is False.')

# Execute parse_args()
args = my_parser.parse_args()

if args.start == 'latest':
    today = datetime.date.today()
    last_monday = today + datetime.timedelta(days=-today.weekday(), weeks=-1)
    fw_start = str(last_monday)    
else:
    fw_start = args.start

# if now --end option is provided, set fw_end to fw_start (eg just run for 1 week)
if args.end:
    fw_end = args.end
else:
    fw_end = fw_start

# threads
if args.threads:
    num_threads = int(args.threads)
else:
    num_threads = 1

if args.worker:
    worker = int(args.worker)
else:
    worker = 0

logging.info("fw_start = " + str(fw_start))
logging.info("fw_end = " + str(fw_end))
logging.info("num_threads = " + str(num_threads))
logging.info("worker = " + str(worker))

logging.info("args.customer_journey_rerun = " + str(args.customer_journey_rerun))
logging.info("args.downstream_process_rerun = " + str(args.downstream_process_rerun))
logging.info("args.mta_rerun = " + str(args.mta_rerun))
logging.info("args.update_final_table = " + str(args.update_final_table))

run_pipeline(fw_start=fw_start, fw_end=fw_end, num_threads=num_threads, thread=worker)
