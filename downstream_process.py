"""


"""
import logging

import google.auth
from google.cloud import bigquery
from google.cloud import bigquery_storage

import utils

def run_downstream_process(fw, bqclient, bqstorageclient, input_data="wx-bq-poc.loyalty_bi_analytics__prod_v3.safarievents_final_", rerun=False):
    """
    Overview
        BQ script https://console.cloud.google.com/bigquery?sq=564585695625:81c04fbfe1544091818d347fd6b5ec79
        Runs downstream process for BWS MTA - CRN level allocation and update of dashboard

        Outputs customer journeys to `wx-bq-poc.digital_attribution_modelling.bws_mta_crn_output_YYYYMMDD`
        Outputs checks to `wx-bq-poc.digital_attribution_modelling.bws_mta_checks_YYYYMMDD`
    
    Arguments
        fw (string) - eg '2021-04-19'
        bqclient - google.cloud.bigquery()
        bqstorageclientgoogle.cloud.bigquery_storage()
        use_dummy_data (boolean) - generate random inc sales for testing purposes. Default is False
        rerun (boolean) - whether to rerun the CJ tables, if they already exist. Default is False
    """
    logging.info("running run_downstream_processes for " + fw)
    fw_no_dash = fw[:4] + fw[5:7] + fw[-2:]

    # check if already ran
    if utils.check_if_BQ_table_exists("wx-bq-poc.digital_attribution_modelling.bws_mta_crn_output_{}".format(fw_no_dash)):
        if rerun:
            logging.warning("rerunning run_downstream_process")
        else:
            logging.warning("skipping run_downstream_process")
            return

    query_string = """
    ------------------------------------------------------
    -- CONVERT EVENTS INTO SHANGLINS EVENTS_MW FORMAT
    ------------------------------------------------------
    create or replace table `wx-bq-poc.digital_attribution_modelling.bws_mta_event_mw_{fw_no_dash}` as (
    with tmp as (
    select
        a.*,
        -- join on campaign end date mapping
        b.campaign_end_date as campaign_end_date_mapped
    from `wx-bq-poc.digital_attribution_modelling.bws_mta_events_{fw_no_dash}` as a
    left join `wx-bq-poc.digital_attribution_modelling.bws_mta_campaign_mapping_{fw_no_dash}` as b
        on a.campaign_code = b.campaign_code and a.campaign_start_date = b.campaign_start_date
    )
    select
        'bws' as banner,
        a.campaign_code,
        b.campaign_type as campaign_type,
        a.campaign_start_date,
        a.campaign_end_date_mapped as campaign_end_date,
        a.crn,
        a.time_utc,
        cast(a.time_utc as DATE) as date,
        case
            when a.event_node like "%Always%" then
                concat(a.campaign_code, '_', a.event_node)
            else
                concat(a.campaign_code, '_', a.channel, '_', a.event_touchpoint)
        end as channel_event,
        cast("2999-12-31" as TIMESTAMP) as conv_time,
        -1 as conv_flag,
        -1 as spend
    from tmp as a
    -- grab TTL flag from Wilson's data
    left join (
        with tmp as (
            select
                campaign_code,
                max(campaign_ttl) as campaign_ttl
            from
                `{input_data}{fw_no_dash}`
            group by 1
            order by 1
        )
        select
            campaign_code,
            case when campaign_ttl = true then "TTL" else "BTL" end as campaign_type
        from tmp
    ) as b
    on a.campaign_code = b.campaign_code
    )
    ;

    ------------------------------------------------------
    -- CONVERT PYTHON MC INTO SHANGLINS MC_FINAL FORMAT
    ------------------------------------------------------
    create or replace table `wx-bq-poc.digital_attribution_modelling.bws_mta_mc_final_{fw_no_dash}` as (
    with tmp as (
    select
        a.*,
        -- join on campaign end date mapping
        b.campaign_end_date,
        REGEXP_EXTRACT(a.channel_name, r'email|rw_app|rw_web_oap|wow_web|sem|youtube|gmail|audio|video|display|google_other|_AlwaysOn_wow_web') as regex_channel,
        REGEXP_EXTRACT(a.channel_name, r'clk|open|imp|view|push_clk|push_imp') as regex_event
    from `wx-bq-poc.digital_attribution_modelling.bws_mta_attribution_{fw_no_dash}` as a
    left join `wx-bq-poc.digital_attribution_modelling.bws_mta_campaign_mapping_{fw_no_dash}` as b
        on a.campaign_code = b.campaign_code and a.campaign_start_date = cast(b.campaign_start_date as string)
    )
    select
        case
            when campaign_end_date is null then
                concat('Total', '_', 'bws', '_', campaign_code, '_', cast(campaign_start_date as string), '_', '2999-12-31')
            else 
                concat('Total', '_', 'bws', '_', campaign_code, '_', cast(campaign_start_date as string), '_', cast(campaign_end_date as string))
        end as key,
        'bws' as banner,
        campaign_code,
        campaign_start_date,
        campaign_end_date,
        concat(campaign_code, '_', regex_channel) as channel,
        -- only different when SEM is involved, but we don't have sem for BWS
        concat(campaign_code, '_', regex_channel) as medium,
        concat(campaign_code, '_', regex_channel, '_', regex_event) as event,
        -- not needed at this stage
        -1 as event_volume,
        -1 as event_reach,
        -1 as event_converted_crn,
        -1 as medium_reach,
        -1 as medium_converted_crn,
        -- add a small amount to all channels to prevent div/0 errors and correct for default channel bias
        normalised_conversion_value + 0.05 as channel_prob_norm,
        regex_channel,
        regex_event
    from tmp
    )
    ;

    -- check extraction went OK
    INSERT INTO `wx-bq-poc.digital_attribution_modelling.bws_mta_checks_{fw_no_dash}`(
        select
        7 as check_number,
        "Number event channels where regex extraction failed" as check,
        (
        (with tmp as (
                select * from `wx-bq-poc.digital_attribution_modelling.bws_mta_mc_final_{fw_no_dash}` where regex_channel is null or regex_event is null
            )
            select count(*) from tmp)
        ) as check_value,
        0 as ideal_check_value    
    )
    ;

    ASSERT
    (
        (select max(check_value) from `wx-bq-poc.digital_attribution_modelling.bws_mta_checks_{fw_no_dash}` where check_number = 7) = 0
    ) AS 'ERROR: regex extract for event and channel failed';


    ------------------------------------------------------
    -- REPLICATE SHANGLINS POST MTA CODE
    ------------------------------------------------------
    create or replace table `wx-bq-poc.digital_attribution_modelling.bws_mta_crn_output_{fw_no_dash}` as
    (

        SELECT distinct
            -- DATE_TRUNC(DATE_ADD((select * from onl_end_date),interval 0 day), week(Monday)) as week
            '{fw}' as week
            , c.crn
            -- , e.segment_cvm        
            -- , e.segment_lifestage
            -- , f.segment_marketable
            , "" as segment_cvm
            , "" as segment_lifestage
            , "" as segment_marketable
            , c.banner
            , c.campaign_code
            , c.campaign_type
            , c.campaign_start_date
            , c.campaign_end_date
            , c.tot_spend as total_sales
            , c.spend as inc_sales
            , channel_event
            , channel_prob_norm
            , case when a.channel is not null then a.channel else d.channel end as channel
            , case when a.medium is not null then a.medium else d.medium end as medium
            , case when a.event is not null then a.event else d.event end as event
            , case when channel_prob_norm is not null then channel_prob_norm / (sum(channel_prob_norm) OVER (PARTITION BY c.crn, b.key)) 
                    when channel_prob_norm is null and channel_event is not null then 0 
                    else 1 end as attributed_conversion
            -- , case when channel_prob_norm is not null then c.tot_spend * channel_prob_norm / (sum(channel_prob_norm) OVER (PARTITION BY c.crn, b.key)) 
            --         when  channel_prob_norm is null and channel_event is not null then 0 
            --         else c.tot_spend end as attributed_total_sales
            -- Rohit says this is not needed
            , -1 as attributed_total_sales
            , case when channel_prob_norm is not null then c.spend * channel_prob_norm / (sum(channel_prob_norm) OVER (PARTITION BY c.crn, b.key)) 
                    WHEN channel_prob_norm is null and channel_event is not null then 0 
                        else c.spend end as attributed_inc_sales
        FROM  -- activations
        (
            SELECT 
                -- sentinel for null campaign_end_date added
                concat('Total', '_', 'bws', '_', campaign_code, '_', cast(campaign_start_date as string), '_', cast(campaign_end_date as string)) as key
                , banner
                , crn
                , campaign_code
                , campaign_start_date
                , campaign_end_date
                , campaign_end_date_real
                , campaign_type
                , case when sum(tot_spend) is null then 0 else sum(tot_spend) end as tot_spend
                , case when sum(spend) is null then 0 else sum(spend) end as spend
            FROM
            (
                SELECT crn
                    , banner
                    , campaign_code
                    , campaign_start_date
                    , campaign_end_date
                    , campaign_end_date_real
                    , campaign_type
                    , min(time_utc) as conv_time
                    , sum(tot_spend) as tot_spend
                    , sum(spend) as spend
                FROM `wx-bq-poc.digital_attribution_modelling.bws_mta_activations_{fw_no_dash}`
                group by 1,2,3,4,5,6,7
            )
            group by 1,2,3,4,5,6,7,8
        ) c 

        -- left join on all touchpoints to activations
        left join
        (        
            SELECT 
                -- blank campaign_end dates due to Wilson's data having nulls for ongoing campaigns
                concat('Total', '_', 'bws', '_', campaign_code, '_', cast(campaign_start_date as string), '_', cast(campaign_end_date as string)) as key
                , crn 
                , channel_event 
                , count(*) as event_volume
            -- REMINDER will need to change later
            from `wx-bq-poc.digital_attribution_modelling.bws_mta_event_mw_{fw_no_dash}`
            group by 1,2,3
            order by 1,2,3
        ) b on b.key = c.key and b.crn = c.crn

        -- left join on MC % channel attribution
        left join 
        (        
            select *
        from `wx-bq-poc.digital_attribution_modelling.bws_mta_mc_final_{fw_no_dash}`
        ) a on a.key = b.key and a.event = b.channel_event 

        left join  -- default channel logic?
        (
            SELECT a1.key
            , CASE WHEN channel is null and campaign_code not like '%NM%' then concat(SUBSTR(campaign_code,1,9), '_email') 
                    WHEN channel is null and campaign_code like '%NM%' then concat(SUBSTR(campaign_code,1,9), '_rw_app') 
                    else channel end as channel
            , CASE WHEN medium is null and campaign_code not like '%NM%' then concat(SUBSTR(campaign_code,1,9), '_email') 
                    WHEN medium is null and campaign_code like '%NM%' then concat(SUBSTR(campaign_code,1,9), '_rw_app') 
                    else medium end as medium
            ,  CASE WHEN event is null and campaign_code not like '%NM%' then concat(SUBSTR(campaign_code,1,9), '_email_open') 
                    WHEN event is null and campaign_code like '%NM%' then concat(SUBSTR(campaign_code,1,9), '_rw_app_imp') 
                    else event end as event
            , event_volume
            , event_reach
            , event_converted_crn
            , medium_reach
            , medium_converted_crn
            FROM
            (
                select distinct key, campaign_code
            from `wx-bq-poc.digital_attribution_modelling.bws_mta_mc_final_{fw_no_dash}`
            ) a1
            -- not sure if below is uised as it relates to online/wow_web_view
            left join
            (    SELECT key
                    , channel
                    , medium
                    , event
                    , event_volume
                    , event_reach
                    , event_converted_crn
                    , medium_reach
                    , medium_converted_crn
            from `wx-bq-poc.digital_attribution_modelling.bws_mta_mc_final_{fw_no_dash}`
                where (campaign_code = 'ONLINE' and event like '%wow_web_view%')
                    or (campaign_code like '%NM%' and regexp_contains(event, r'(?i)rw_app_imp') and substr(event,1,9) = substr(campaign_code,1,9))
            or (campaign_code not like '%NM%' and campaign_code <> 'ONLINE' and regexp_contains(event, r'(?i)email_open') and substr(event,1,9) = substr(campaign_code,1,9))
            ) a2 on a1.key = a2.key

        ) d on c.key = d.key

        -- left join on CVM/marketable, not needed anymore
        -- left join
        -- (
        --     SELECT crn, max(macro_segment_curr) as segment_cvm, max(lifestage) as segment_lifestage
        --     FROM `gcp-wow-rwds-ai-safari-prod.wdp_tables.redx_loyalty_customer_value_model`
        --     where Date(pw_end_date) = (select max(Date(pw_end_date)) from `gcp-wow-rwds-ai-safari-prod.wdp_tables.redx_loyalty_customer_value_model`)
        --     group by 1
        -- ) e on c.crn = e.crn

        -- left join
        -- (
        --     SELECT crn, max(marketable) as segment_marketable
        --     group by 1
        -- ) f on c.crn = f.crn

    )
    ;

    ------------------------------------------------------
    -- FINAL CHECKS
    ------------------------------------------------------
    -- compare raw MC attribution to final CRN level attribution
    create or replace table `wx-bq-poc.digital_attribution_modelling.bws_mta_comparison_to_MC_{fw_no_dash}` as (
    with tmp as (
        select 
            campaign_code,
            campaign_start_date,
            campaign_end_date,
            -- channel_event,
            -- channel,
            medium,
            -- event,
            sum(attributed_inc_sales) as attributed_inc_sales,
            count(*) as count
        from `wx-bq-poc.digital_attribution_modelling.bws_mta_crn_output_{fw_no_dash}`
        group by 1,2,3,4
        order by 1,2,3,4
    )
    , tmp2 as (
        select 
            campaign_code,
            campaign_start_date,
            campaign_end_date,
            medium,
            -- event,
            -- sum(attributed_inc_sales) as inc_sales,
            -- count(*) as count,
            attributed_inc_sales,
            sum(attributed_inc_sales)
        over
            (partition by campaign_code, campaign_start_date, campaign_end_date) as inc_sales_campaign
        from tmp
    ), python_mc_attribution as (
        select 
            campaign_code,
            campaign_start_date,
            campaign_end_date,
            medium,
            sum(channel_prob_norm) as channel_prob_norm,
            count(*) as count
        from `wx-bq-poc.digital_attribution_modelling.bws_mta_mc_final_{fw_no_dash}`
        group by 1,2,3,4
        order by 1,2,3,4
    )
    select
        a.campaign_code,
        a.campaign_start_date,
        a.campaign_end_date,
        a.medium,
        a.attributed_inc_sales,
        a.inc_sales_campaign,
        -- allowance for $0 inc sales, to prevent div/0 error
        case
            when a.inc_sales_campaign = 0 then
                -- sentinel to indicate 0 inc sales
                -1
            else
                round(a.attributed_inc_sales / a.inc_sales_campaign, 3)
        end as post_MTA_inc_sales_attribution,
        round(b.channel_prob_norm, 3) as MC_attribution
    from tmp2 as a
    left join python_mc_attribution as b
        on a.campaign_code = b.campaign_code
            and a.campaign_start_date = cast(b.campaign_start_date as DATE)
            and a.campaign_end_date = cast(b.campaign_end_date as DATE)
            and a.medium = b.medium
    order by 1,2,3,4,5
    );

    -- check totals by campaign code pre and post MTA
    INSERT INTO `wx-bq-poc.digital_attribution_modelling.bws_mta_checks_{fw_no_dash}`(
        with tmp1 as (
        select 
            campaign_code,
            campaign_start_date,
            campaign_end_date,
            sum(attributed_inc_sales) as inc_sales,
            count(*) as count
        from `wx-bq-poc.digital_attribution_modelling.bws_mta_crn_output_{fw_no_dash}`
        group by 1,2,3
        order by 1,2,3
        ), tmp2 as(
        select 
            campaign_code,
            campaign_start_date,
            campaign_end_date,
            sum(spend) as inc_sales,
            count(*) as count
        from `wx-bq-poc.digital_attribution_modelling.bws_mta_activations_{fw_no_dash}`
        group by 1,2,3
        order by 1,2,3
        ), summary as (
        select 
            coalesce(a.campaign_code, b.campaign_code) as campaign_code,
            coalesce(a.campaign_start_date, b.campaign_start_date) as campaign_start_date,
            coalesce(a.campaign_end_date, b.campaign_end_date) as campaign_end_date,
            b.inc_sales as inc_sales_pre_MTA,
            a.inc_sales as inc_sales_post_MTA,
            case
                -- when either inc sales values are so small, it doesn't matter. also covers the div/0 error
                when (abs(a.inc_sales) < 100 and abs(b.inc_sales) < 100) then 0
                -- round to 3 DP
                else round(a.inc_sales / b.inc_sales - 1, 3)
            end as difference
        from tmp1 as a
        full outer join tmp2 as b
            on a.campaign_code = b.campaign_code and a.campaign_start_date = b.campaign_start_date and a.campaign_end_date = b.campaign_end_date
        )
        select
            8 as check_number,    
            concat(campaign_code, " ", campaign_start_date, " ", campaign_end_date, " | inc_sales_pre_MTA: ", round(inc_sales_pre_MTA), " | inc_sales_post_MTA: ", round(inc_sales_post_MTA), "... post / pre - 1 = ") as check,
            difference as check_value,
            0 as ideal_check_value
        from summary
    )
    ;

    ASSERT
    (
        (select max(check_value) from `wx-bq-poc.digital_attribution_modelling.bws_mta_checks_{fw_no_dash}` where check_number = 8) = 0
    ) AS 'ERROR: inc sales totals pre and post MTA do not match at a campaign code campaign start date level';

    -- return checks
    select * from `wx-bq-poc.digital_attribution_modelling.bws_mta_checks_{fw_no_dash}`;

    """.format(fw=fw, fw_no_dash=fw_no_dash, input_data=input_data)

    df_bq = (
    bqclient.query(query_string)
    .result()
    .to_dataframe(bqstorage_client=bqstorageclient)
    )

    logging.info("ran run_downstream_processes for " + fw)
    logging.info("outputted to: `wx-bq-poc.digital_attribution_modelling.bws_mta_crn_output_" + fw_no_dash + "`")
    logging.info("see this for checks: `wx-bq-poc.digital_attribution_modelling.bws_mta_checks_" + fw_no_dash + "`")
    logging.info("dashboard updated: `wx-bq-poc.personal.AL_MTA_dashboard`")
    logging.info("dashboard updated: https://datastudio.google.com/reporting/47182796-a5f5-4401-afe0-c31d5dc6d2b6/page/PEWWC")
    
    # log checks
    logging.info(str(df_bq))

    return df_bq

if __name__ == "__main__":
    ##################################################
    # LOGGING
    ##################################################
    # logging.basicConfig(level=logging.DEBUG)
    logFormatter = logging.Formatter("%(asctime)s [%(levelname)-7.7s]  %(message)s")
    rootLogger = logging.getLogger()
    rootLogger.setLevel(logging.INFO)  # need to set root logger to the lowest level
    
    # grab credentials from default login, use gcloud auth login
    credentials, your_project_id = google.auth.default(
        scopes=["https://www.googleapis.com/auth/cloud-platform"]
    )
    
    # Make clients.
    bqclient = bigquery.Client(credentials=credentials, project='wx-bq-poc',)
    bqstorageclient = bigquery_storage.BigQueryReadClient(credentials=credentials)
    FW = '2021-04-19'
    run_downstream_process(fw=FW, use_dummy_data=False)
