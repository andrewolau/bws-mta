aiohttp==3.7.4.post0
asn1crypto==0.24.0
async-timeout==3.0.1
attrs==21.2.0
cachetools==4.2.2
certifi==2021.5.30
cffi==1.12.2
ChannelAttribution==2.0.10
chardet==3.0.4
# conda==4.10.3
# conda-package-handling==1.7.3
cryptography==2.6.1
cycler==0.10.0
decorator==5.0.9
fsspec==2021.8.1
gcsfs==2021.8.1
google-api-core==2.0.1
google-api-python-client==2.20.0
google-auth==2.0.2
google-auth-httplib2==0.1.0
google-auth-oauthlib==0.4.6
google-cloud-bigquery==2.26.0
google-cloud-bigquery-storage==2.7.0
google-cloud-core==2.0.0
google-crc32c==1.1.2
google-resumable-media==2.0.2
googleapis-common-protos==1.53.0
grpcio==1.40.0
httplib2==0.19.1
idna==2.8
joblib==1.0.1
kiwisolver==1.3.2
libcst==0.3.20
matplotlib==3.4.3
multidict==5.1.0
mypy-extensions==0.4.3
numpy==1.21.2
oauthlib==3.1.1
packaging==21.0
pandas==1.3.2
pandas-gbq==0.15.0
Pillow==8.3.2
progressbar2==3.53.1
proto-plus==1.19.0
protobuf==3.17.3
pyarrow==5.0.0
pyasn1==0.4.8
pyasn1-modules==0.2.8
pycosat==0.6.3
pycparser==2.19
pydata-google-auth==1.2.0
pyOpenSSL==19.0.0
pyparsing==2.4.7
PySocks==1.6.8
python-dateutil==2.8.2
python-utils==2.5.6
pytz==2021.1
PyYAML==5.4.1
requests==2.21.0
requests-oauthlib==1.3.0
rsa==4.7.2
ruamel-yaml==0.15.46
scikit-learn==0.24.2
scipy==1.7.1
seaborn==0.11.2
six==1.12.0
threadpoolctl==2.2.0
torch==1.9.0
torchvision==0.10.0
tqdm==4.62.1
typing-extensions==3.10.0.2
typing-inspect==0.7.1
uritemplate==3.0.1
urllib3==1.24.1
yarl==1.6.3
