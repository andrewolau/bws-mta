
"""
Summarises BWS campaign attribution data for presentation.

Run the below afterwards for a comparison with the legacy method
https://console.cloud.google.com/bigquery?sq=564585695625:43af5419e5264ade8b5011e075fc0dbf

Write to `gcp-wow-rwds-ai-safari-prod.safari_campaign_attribution.bws_campaign_attribution_summary`
https://console.cloud.google.com/bigquery?sq=564585695625:c825382b2091462c93920d80ba350c56
Author
    Andrew Lau
"""
import os
import logging
import datetime

import google.auth
from google.cloud import bigquery
from google.cloud import bigquery_storage

import email_notifications
import utils

def summarise_campaign_attribution(fw,
                                    input_data="wx-bq-poc.loyalty_bi_analytics__prod_v3.safarievents_final_bws_",
                                    bqclient=None,
                                    bqstorageclient=None):
    """
    Overview
        Summarises BWS campaign attribution data for presentation.
        Write to `gcp-wow-rwds-ai-safari-prod.safari_campaign_attribution.bws_campaign_attribution_summary`
        https://console.cloud.google.com/bigquery?sq=564585695625:c825382b2091462c93920d80ba350c56
    Arguments
        fw - financial week, eg '2021-04-19'
        input_data - safari input data location, use safarievents_final_bws_ for FY20/21
    Returns
    """
    logging.info("running summarise_campaign_attribution for " + fw)
    fw_no_dash = fw[:4] + fw[5:7] + fw[-2:]

    query_string = """
    --------------------------------------------------
    -- INSERT A FW
    --------------------------------------------------
    -- delete any existing records from that FW
    delete from `gcp-wow-rwds-ai-safari-prod.safari_campaign_attribution.bws_campaign_attribution_summary`
    where fw_start_date = '{fw}';

    -- summarise campaign results and insert into summary table
    insert into `gcp-wow-rwds-ai-safari-prod.safari_campaign_attribution.bws_campaign_attribution_summary` (
        with camp as (
            SELECT
                event,
                sum(attributed_inc_sales) as attributed_inc_sales
            FROM
            # `wx-bq-poc.digital_attribution_modelling.bws_campaign_attribution_output_{fw_no_dash}`
            (
                with redeemers as (
                    -- anyone that redeemed at all in the FW
                    select distinct
                        CAST(fw_start_date AS TIMESTAMP) as fw_start_date,
                        crn 
                    from
                        `gcp-wow-rwds-ai-safari-prod.safari_campaign_attribution.bws_camp_events_{fw_no_dash}`
                    where
                        event is not NULL
                        and redemption_flag = 1
                )
                -- join on campaign attribution results to redeemers
                select
                    redeemers.fw_start_date,
                    redeemers.crn,
                    attr.event,
                    attr.attributed_inc_sales
                from 
                    redeemers
                inner join `wx-bq-poc.digital_attribution_modelling.bws_campaign_attr_output_{fw_no_dash}` as attr
                    on cast(redeemers.fw_start_date as date) = cast(attr.fw_start_date as date) and redeemers.crn = attr.crn
            )
            group by 1
            order by 1
        -- tmp tables for event -> campaign_code/campaign_start_date mapping
        ), tmp_1 as (
            SELECT
                distinct event,
                campaign_code,
                TTL_flag,
                campaign_start_date,
            FROM `wx-bq-poc.digital_attribution_modelling.bws_targeted_sent_{fw_no_dash}`
        ), tmp_2 as (
            SELECT
                distinct event,
                campaign_code,
                TTL_flag,
                campaign_start_date,
            FROM `wx-bq-poc.digital_attribution_modelling.bws_activations_opens_{fw_no_dash}`
        )
        , mapping as (
            select
                event,
                campaign_code,
                -- there are campaign events with >1 campaign_start date
                -- eg 1 wk campaign starts and ends in the same week
                max(TTL_flag) as TTL_flag,
                min(campaign_start_date) as campaign_start_date
            from (
                select * from tmp_1
                union all
                select * from tmp_2
            )
            group by 1,2
            order by 1,2
        )
        -- flatten down the event dimension
        select
            '{fw}' as fw_start_date,
            b.campaign_code,
            REGEXP_EXTRACT(b.campaign_code, '[A-Z][A-Z][A-Z]') as campaign_type,
            b.campaign_start_date,
            b.TTL_flag,
            sum(a.attributed_inc_sales) as attributed_inc_sales
        from camp as a
        left join mapping as b
            on a.event = b.event
        group by 1,2,3,4,5
    );
    -- sum check that difference is less than $5
    assert (
        abs((SELECT
            sum(attributed_inc_sales) as attributed_inc_sales
        FROM
            # `wx-bq-poc.digital_attribution_modelling.bws_campaign_attribution_output_{fw_no_dash}`)
            (
                with redeemers as (
                    -- anyone that redeemed at all in the FW
                    select distinct
                        CAST(fw_start_date AS TIMESTAMP) as fw_start_date,
                        crn 
                    from
                        `gcp-wow-rwds-ai-safari-prod.safari_campaign_attribution.bws_camp_events_{fw_no_dash}`
                    where
                        event is not NULL
                        and redemption_flag = 1
                )
                -- join on campaign attribution results to redeemers
                select
                    redeemers.fw_start_date,
                    redeemers.crn,
                    attr.event,
                    attr.attributed_inc_sales
                from 
                    redeemers
                inner join `wx-bq-poc.digital_attribution_modelling.bws_campaign_attr_output_{fw_no_dash}` as attr
                    on cast(redeemers.fw_start_date as date) = cast(attr.fw_start_date as date) and redeemers.crn = attr.crn
            )
        )
        -
        (SELECT
            sum(attributed_inc_sales) as attributed_inc_sales
        FROM
            `gcp-wow-rwds-ai-safari-prod.safari_campaign_attribution.bws_campaign_attribution_summary`
        where fw_start_date = '{fw}')) < 5
    );

    """.format(fw=fw, fw_no_dash=fw_no_dash, input_data=input_data)
    logging.debug(str(query_string))
    df_bq = (
    bqclient.query(query_string)
    .result()
    .to_dataframe(bqstorage_client=bqstorageclient)
    )

    logging.info("ran generate_activation_opens_data for " + fw)

    return df_bq

if __name__ == "__main__":
    FW_START = '2020-06-29'
    FW_END = '2021-06-28'
    FW_MAX = 200
    INPUT_DATA = "wx-bq-poc.loyalty_bi_analytics__prod_v3.safarievents_final_bws_"
    BQ_PROJECT='gcp-wow-rwds-ai-safari-prod'

    ##################################################
    # LOGGING
    ##################################################
    # logging.basicConfig(level=logging.DEBUG)
    logFormatter = logging.Formatter("%(asctime)s [%(levelname)-7.7s]  %(message)s")
    rootLogger = logging.getLogger()
    rootLogger.setLevel(logging.INFO)  # need to set root logger to the lowest level

    # set user credentials to create BQ client object
    

    os.environ['GOOGLE_APPLICATION_CREDENTIALS']="adc.json" 
    os.environ['GOOGLE_CLOUD_PROJECT']=BQ_PROJECT
    logging.info("os.environ['GOOGLE_CLOUD_PROJECT'] = " + str(os.environ['GOOGLE_CLOUD_PROJECT']))

    # grab credentials from default login, use gcloud auth login
    credentials, your_project_id = google.auth.default(
        scopes=["https://www.googleapis.com/auth/cloud-platform"]
    )
    
    # Make clients.
    bqclient = bigquery.Client(credentials=credentials, project=BQ_PROJECT,)
    bqstorageclient = bigquery_storage.BigQueryReadClient(credentials=credentials)

    datetime_object = datetime.datetime.strptime(FW_START, '%Y-%m-%d')

    for weeks_to_add in range(FW_MAX):
        datetime_object_plus_weeks = datetime_object + datetime.timedelta(weeks=weeks_to_add)
        # break when reached last fw to parse
        if datetime_object_plus_weeks > datetime.datetime.strptime(FW_END, '%Y-%m-%d'):
            logging.info("breaking loop as FW_END reached")
            break
        fw = datetime_object_plus_weeks.strftime("%Y-%m-%d")
        fw_no_dash = fw[:4] + fw[5:7] + fw[-2:]
        
        summarise_campaign_attribution(fw, input_data=INPUT_DATA, bqclient=bqclient, bqstorageclient=bqstorageclient)

        logging.info("run complete for {fw_no_dash}`".format(fw_no_dash=fw_no_dash))
