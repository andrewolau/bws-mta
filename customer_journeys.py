"""
Overview
    Creates customer journeys in the ChannelAttribution format for MTA
    Outputs customer journeys to `wx-bq-poc.digital_attribution_modelling.bws_mta_customer_journeys_YYYYMMDD`
    Outputs checks to `wx-bq-poc.digital_attribution_modelling.bws_mta_checks_YYYYMMDD`
Usage    
    import customer_journeys
    customer_journeys.(fw, bqclient, bqstorageclient, use_dummy_data=False, rerun=False, use_dummy_date_extended=False)
Author
    Andrew Lau
"""
import logging
import google.auth
from google.cloud import bigquery
from google.cloud import bigquery_storage

import utils

def create_customer_journeys(fw, bqclient, bqstorageclient,
                            input_data="wx-bq-poc.loyalty_bi_analytics__prod_v3.safarievents_final_",
                            use_dummy_data=False, rerun=False, use_dummy_date_extended=False):
    """
    Overview
        BQ script https://console.cloud.google.com/bigquery?sq=564585695625:563d37ac95354f1e88bc5aec03fcc6cb
        Generates customer journeys for ChannelAttribution's MTA

        Outputs customer journeys to `wx-bq-poc.digital_attribution_modelling.bws_mta_customer_journeys_YYYYMMDD`
        Outputs checks to `wx-bq-poc.digital_attribution_modelling.bws_mta_checks_YYYYMMDD`
    
    Arguments
        fw (string) - eg '2021-04-19'
        bqclient - google.cloud.bigquery()
        bqstorageclientgoogle.cloud.bigquery_storage()
        use_dummy_data (boolean) - generate random inc sales for testing purposes. Default is False    
        rerun (boolean) - whether to rerun the CJ tables, if they already exist. Default is False
    """
    logging.info("running create_customer_journeys for " + fw)
    fw_no_dash = fw[:4] + fw[5:7] + fw[-2:]

    if use_dummy_data:
        dummy_data_comment = ""
        real_data_comment = "--"
        if use_dummy_date_extended:
            dummy_data_extended_comment = ""
        else:
            dummy_data_extended_comment = "--"
    else:
        dummy_data_comment = "--"
        dummy_data_extended_comment = "--"
        real_data_comment = ""

    # check if already ran
    if utils.check_if_BQ_table_exists("wx-bq-poc.digital_attribution_modelling.bws_mta_customer_journeys_{}".format(fw_no_dash)):
        if rerun:
            logging.warning("rerunning create_customer_journeys")
        else:
            logging.warning("skipping create_customer_journeys")
            return

    query_string = """
    ------------------------------------------------------
    -- CAMPAIGN END DATE MAPPING
    ------------------------------------------------------
    -- mapping for campaign code/campaign start date to campaign end date, one source of truth
    -- coalesce all the data sources and if they are all null, use sentinel 2999-12-31
    create or replace table `wx-bq-poc.digital_attribution_modelling.bws_mta_campaign_mapping_{fw_no_dash}` as (
    with act_open as (
    SELECT
    campaign_code,
    campaign_start_date,
    campaign_end_date,
    COUNT(*) as count
    FROM `wx-bq-poc.digital_attribution_modelling.bws_activations_opens_{fw_no_dash}`
    GROUP BY
    1,
    2,
    3
    ORDER BY
    1,
    2,
    3
    ), targ_sent as (
    SELECT
    campaign_code,
    campaign_start_date,
    campaign_end_date,
    COUNT(*) as count
    FROM `wx-bq-poc.digital_attribution_modelling.bws_targeted_sent_{fw_no_dash}`
    GROUP BY
    1,
    2,
    3
    ORDER BY
    1,
    2,
    3
    )
    select
        coalesce(a.campaign_code, b.campaign_code) as campaign_code,
        coalesce(a.campaign_start_date, b.campaign_start_date) as campaign_start_date,
        coalesce(a.campaign_end_date, b.campaign_end_date, '2999-12-31') as campaign_end_date,
        a.campaign_end_date as campaign_end_date_act_open,
        b.campaign_end_date as campaign_end_date_targ_sent,
        a.count as count_act_open,
        b.count as count_targ_sent,
        case when (a.campaign_end_date is not null and b.campaign_end_date is not null) and (a.campaign_end_date != b.campaign_end_date) then 1 else 0 end as error_inconsistent_end_dates
    from act_open as a
    full outer join targ_sent as b
        on a.campaign_code = b.campaign_code and a.campaign_start_date = b.campaign_start_date
    order by coalesce(a.campaign_code, b.campaign_code), coalesce(a.campaign_start_date, b.campaign_start_date)
    )
    ;

    -- check uniqueness of mapping
    create or replace table `wx-bq-poc.digital_attribution_modelling.bws_mta_checks_{fw_no_dash}` as (
    -- INSERT INTO `wx-bq-poc.digital_attribution_modelling.bws_mta_checks_{fw_no_dash}`(
        select
        1 as check_number,
        "Number of campaign_codes, campaign_start_dates that do not have a unique mapping to TTL flag and end date" as check,
        (with tmp as (
            select campaign_code, campaign_start_date, count(*) as count from `wx-bq-poc.digital_attribution_modelling.bws_mta_campaign_mapping_{fw_no_dash}` group by 1,2
        )
        select cast(count(*) as FLOAT64) from tmp where count > 1) as check_value,
        0 as ideal_check_value    
    )
    ;

    ASSERT
    (
        (select check_value from `wx-bq-poc.digital_attribution_modelling.bws_mta_checks_{fw_no_dash}` where check_number = 1) = 0
    ) AS 'ERROR: campaign code, campaign start date to campaign end date mapping is not unique';


    --------------------------------------------------
    -- FAKE INC SALES DATA FOR TESTING
    --------------------------------------------------
    -- collapse week level allocation into crn campaign, campaign start date level
    {dummy_data_comment}create or replace table `wx-bq-poc.digital_attribution_modelling.bws_mta_activations_{fw_no_dash}` as (
    {dummy_data_comment}select
    {dummy_data_comment}a.crn
    {dummy_data_comment}, 'bws' as banner
    {dummy_data_comment}, a.campaign_code
    {dummy_data_comment}, a.campaign_start_date
    {dummy_data_comment}, coalesce(b.campaign_end_date, '2999-12-31') as campaign_end_date
    {dummy_data_comment}-- End date real: appears that campaign_end_date is the min of the end of the fw and campaign_end_date_real
    {dummy_data_comment}, coalesce(b.campaign_end_date, '2999-12-31') as campaign_end_date_real
    {dummy_data_comment}, case when a.TTL_flag = 1 then 'TTL' else 'BTL' end as campaign_type
    {dummy_data_comment}, -1 as tot_spend  -- not needed, added sentinel
    {dummy_data_comment}-- get earliest redemption
    {dummy_data_comment}, min(a.ts) as time_utc
    {dummy_data_comment}, cast(min(a.ts) as DATE) as date
    {dummy_data_comment}-- REMINDER: CHECK WITH SHEN WHETHER TO SUM OR NOT
    {dummy_data_comment}, 25 * RAND() as spend
    {dummy_data_comment}, count(*) as count
    {dummy_data_comment}from (
    {dummy_data_comment}    select * from `wx-bq-poc.digital_attribution_modelling.bws_activations_opens_{fw_no_dash}`
    {dummy_data_extended_comment}    union all
    {dummy_data_extended_comment}    select * from `wx-bq-poc.digital_attribution_modelling.bws_targeted_sent_{fw_no_dash}`
    {dummy_data_comment}    )
    {dummy_data_comment}as a
    {dummy_data_comment}left join `wx-bq-poc.digital_attribution_modelling.bws_mta_campaign_mapping_{fw_no_dash}` as b
    {dummy_data_comment}    on a.campaign_code = b.campaign_code and a.campaign_start_date = b.campaign_start_date
    {dummy_data_comment}group by 1,2,3,4,5,6,7,8
    {dummy_data_comment}order by 1,2,3,4,5,6,7,8
    {dummy_data_comment});

    --------------------------------------------------
    -- CONVERT SHENS INC SALES DATA INTO SHANGLINS ACTIVATIONS FORMAT
    --------------------------------------------------
    -- collapse week level allocation into crn campaign, campaign start date level
    {real_data_comment}create or replace table `wx-bq-poc.digital_attribution_modelling.bws_mta_activations_{fw_no_dash}` as (
    {real_data_comment}    -- tmp tables for event -> campaign_code/campaign_start_date mapping
    {real_data_comment}    with tmp_1 as (
    {real_data_comment}        SELECT
    {real_data_comment}           distinct event,
    {real_data_comment}            campaign_code,
    {real_data_comment}            TTL_flag,
    {real_data_comment}            campaign_start_date,
    {real_data_comment}        FROM `wx-bq-poc.digital_attribution_modelling.bws_targeted_sent_{fw_no_dash}`
    {real_data_comment}    )
    {real_data_comment}    , tmp_2 as (
    {real_data_comment}        SELECT
    {real_data_comment}            distinct event,
    {real_data_comment}            campaign_code,
    {real_data_comment}            TTL_flag,
    {real_data_comment}            campaign_start_date,
    {real_data_comment}        FROM `wx-bq-poc.digital_attribution_modelling.bws_activations_opens_{fw_no_dash}`
    {real_data_comment}    )
    {real_data_comment}    -- event -> campaign_code, campaign_start_date, TTL_flag mapping
    {real_data_comment}    , mapping as (
    {real_data_comment}        select
    {real_data_comment}            event,
    {real_data_comment}            campaign_code,
    {real_data_comment}            -- there are campaign events with >1 campaign_start date
    {real_data_comment}            -- eg 1 wk campaign starts and ends in the same week
    {real_data_comment}            max(TTL_flag) as TTL_flag,
    {real_data_comment}            min(campaign_start_date) as campaign_start_date
    {real_data_comment}        from (
    {real_data_comment}            select * from tmp_1
    {real_data_comment}            union all
    {real_data_comment}            select * from tmp_2
    {real_data_comment}        )
    {real_data_comment}        group by 1,2
    {real_data_comment}        order by 1,2
    {real_data_comment}    )
    {real_data_comment}    select
    {real_data_comment}        a.crn
    {real_data_comment}        , 'bws' as banner
    {real_data_comment}        , a.campaign_code
    {real_data_comment}        , a.campaign_start_date
    {real_data_comment}        , coalesce(b.campaign_end_date, '2999-12-31') as campaign_end_date
    {real_data_comment}        -- End date real: appears that campaign_end_date is the min of the end of the fw and campaign_end_date_real
    {real_data_comment}        , coalesce(b.campaign_end_date, '2999-12-31') as campaign_end_date_real
    {real_data_comment}        , case when a.TTL_flag = 1 then 'TTL' else 'BTL' end as campaign_type
    {real_data_comment}        , -1 as tot_spend  -- not needed, added sentinel
    {real_data_comment}        -- get earliest redemption
    {real_data_comment}        -- shen's table only has fw_start_date!
    {real_data_comment}        , min(a.fw_start_date) as time_utc
    {real_data_comment}        , cast(min(a.fw_start_date) as DATE) as date
    {real_data_comment}        -- REMINDER: CHECK WITH SHEN WHETHER TO SUM OR NOT
    {real_data_comment}        , sum(a.attributed_inc_sales) as spend
    {real_data_comment}        , count(*) as count
    {real_data_comment}    from (
    {real_data_comment}            select
    {real_data_comment}                camp.*,
    {real_data_comment}                mapping.campaign_code,
    {real_data_comment}                mapping.TTL_flag,
    {real_data_comment}                mapping.campaign_start_date
    {real_data_comment}            from
    {real_data_comment}                -- `wx-bq-poc.digital_attribution_modelling.bws_activations_opens_{fw_no_dash}`
    {real_data_comment}                -- `wx-bq-poc.digital_attribution_modelling.bws_campaign_attribution_output_{fw_no_dash}`
    {real_data_comment}                 (
    {real_data_comment} 					with redeemers as (
    {real_data_comment} 					-- anyone that redeemed at all in the FW
    {real_data_comment} 					select distinct
    {real_data_comment} 					CAST(fw_start_date AS TIMESTAMP) as fw_start_date,
    {real_data_comment} 					crn 
    {real_data_comment} 					from
    {real_data_comment}                     -- NEED TO CHANGE THIS LATER
    {real_data_comment} 					`gcp-wow-rwds-ai-safari-prod.safari_campaign_attribution.bws_camp_events_{fw_no_dash}`  -- NEED TO CHANGE THIS LATER
    {real_data_comment} 					where
    {real_data_comment} 					event is not NULL
    {real_data_comment} 					and redemption_flag = 1
    {real_data_comment} 					)
    {real_data_comment} 					-- join on campaign attribution results to redeemers
    {real_data_comment} 					select
    {real_data_comment} 					redeemers.fw_start_date,
    {real_data_comment} 					redeemers.crn,
    {real_data_comment} 					attr.event,
    {real_data_comment} 					attr.attributed_inc_sales
    {real_data_comment} 					from 
    {real_data_comment} 					redeemers
    {real_data_comment} 					inner join `wx-bq-poc.digital_attribution_modelling.bws_campaign_attr_output_{fw_no_dash}` as attr
    {real_data_comment} 					on cast(redeemers.fw_start_date as date) = cast(attr.fw_start_date as date) and redeemers.crn = attr.crn
    {real_data_comment}                 ) as camp
    {real_data_comment}            left join
    {real_data_comment}                mapping
    {real_data_comment}                    on camp.event = mapping.event
    {real_data_comment}        )
    {real_data_comment}    as a
    {real_data_comment}    left join `wx-bq-poc.digital_attribution_modelling.bws_mta_campaign_mapping_{fw_no_dash}` as b
    {real_data_comment}        on a.campaign_code = b.campaign_code and a.campaign_start_date = b.campaign_start_date
    {real_data_comment}    group by 1,2,3,4,5,6,7,8
    {real_data_comment}    order by 1,2,3,4,5,6,7,8
    {real_data_comment});

    -- check uniqueness by key
    INSERT INTO `wx-bq-poc.digital_attribution_modelling.bws_mta_checks_{fw_no_dash}`(
        select
        2 as check_number,
        "Number of activations that are not unique by the join key" as check,
        (
            with tmp as (
            select 
            crn
            , banner
            , campaign_code
            , campaign_start_date
            , campaign_end_date
            , campaign_end_date_real
            , campaign_type
            -- can be multiple entries as there is an entry for each campaign_week_nbr
            , count(*)
            from `wx-bq-poc.digital_attribution_modelling.bws_mta_activations_{fw_no_dash}`
            group by 1,2,3,4,5,6,7
            ), tmp2 as (
            -- now group by key and see if it is unique
            select
            crn
            , banner
            , campaign_code
            , campaign_start_date
            , count(*) as count
            from tmp
            group by 1,2,3,4
            )
            select count(*) from tmp2 where count != 1
        ) as check_value,
        0 as ideal_check_value    
    )
    ;

    ASSERT
    (
        (select max(check_value) from `wx-bq-poc.digital_attribution_modelling.bws_mta_checks_{fw_no_dash}` where check_number = 2) = 0
    ) AS 'ERROR: activations are not unique by the join key';

    --------------------------------------------------
    -- EMAIL OPENS FROM WILSONS DATA
    --------------------------------------------------
    -- grab email opens/sent from Wilson's data
    create or replace table `wx-bq-poc.personal.AL_tmp_bws_email_open_{fw_no_dash}` as (
    select
        distinct cast(crn as STRING) as crn,
        campaign_code,
        campaign_start_date,
        campaign_end_date,    
        fw_start_date,
        -- no need to take max of fw start date and ts here as we want real touchpoints order
        open_datetime as ts
    from
        `{input_data}{fw_no_dash}`
    where
        fw_start_date = '{fw}' and
        division_name = 'BWS' and
        cast(crn as STRING) != '0' and
        open_datetime is not null
    )
    ;

    --------------------------------------------------
    -- wow_web 
    --------------------------------------------------
    create or replace table `wx-bq-poc.personal.AL_tmp_bws_wow_web_{fw_no_dash}` as (
        SELECT distinct 
            crn
            , time_utc
            , date
            , banner
            , 'wow_web' as channel
            , case when event_name in ('page view','search view') then 'view'
                    else 'clk' end as event
            , campaign_code_regex as campaign_code
            , cast(null as date) as campaign_start_date
            , cast(null as date) as campaign_end_date
            , CONCAT('tealium_event=',tealium_event,';url=',url) as attributes
        FROM
        (
            SELECT *, rank() over (partition by crn, session_id, event_name, url order by time_utc asc) as event_rank
            FROM 
            (
                SELECT *
                    , json_extract(attributes, "$.udo_tealium_session_id[0]") as session_id
                    , REPLACE(json_extract(attributes, "$.dom_url[0]"), '"', '') as url
                    , REPLACE(json_extract(attributes, "$.udo_tealium_event[0]"), '"', '') as tealium_event
                    , REGEXP_EXTRACT(attributes, r"[A-Z][A-Z][A-Z]-[0-9][0-9][0-9][0-9]") as campaign_code_regex
                FROM `wx-bq-poc.wx_lty_digital_attribution_dev.eventstore_events` 
                where
                    channel = 'dx' and event_name in ('page view', 'search view', 'click view')
                    -- smaller time window -> faster
                    and cast(time_utc as date) >= date_sub('{fw}', INTERVAL 45 DAY) and cast(time_utc as date) <= date_add('{fw}', INTERVAL 52 DAY)    
                    -- wow web with campaign code match
                    and REGEXP_EXTRACT(attributes, r"[A-Z][A-Z][A-Z]-[0-9][0-9][0-9][0-9]") is not null
            )
        )
        where event_rank = 1    
    );
    --------------------------------------------------
    -- WOW WEB LIQUOR TOUCHPOINTS EXTRACTION
    --------------------------------------------------
    -- always on
    create or replace table `wx-bq-poc.personal.AL_tmp_bws_wow_web_always_on_{fw_no_dash}` as (
        SELECT distinct 
            crn
            , time_utc
            , date
            , banner
            , 'wow_web' as channel
            , case when event_name in ('page view','search view') then 'view'
                    else 'clk' end as event
            , '_AlwaysOn' as campaign_code
            , cast(null as date) as campaign_start_date
            , cast(null as date) as campaign_end_date
            , CONCAT('tealium_event=',tealium_event,';url=',url) as attributes
        FROM
        (
            SELECT *, rank() over (partition by crn, session_id, event_name, url order by time_utc asc) as event_rank
            FROM 
            (
                SELECT *
                    , json_extract(attributes, "$.udo_tealium_session_id[0]") as session_id
                    , REPLACE(json_extract(attributes, "$.dom_url[0]"), '"', '') as url
                    , REPLACE(json_extract(attributes, "$.udo_tealium_event[0]"), '"', '') as tealium_event
                    , REGEXP_EXTRACT(attributes, r"[A-Z][A-Z][A-Z]-[0-9][0-9][0-9][0-9]") as campaign_code_regex
                FROM `wx-bq-poc.wx_lty_digital_attribution_dev.eventstore_events` 
                where
                    channel = 'dx' and event_name in ('page view', 'search view', 'click view')
                    -- smaller time window -> faster
                    and cast(time_utc as date) >= date_sub('{fw}', INTERVAL 45 DAY) and cast(time_utc as date) <= date_add('{fw}', INTERVAL 52 DAY)    
                    -- always on - when we cannot find a campaign code
                    and REGEXP_EXTRACT(attributes, r"[A-Z][A-Z][A-Z]-[0-9][0-9][0-9][0-9]") is null
                    -- extract only liquor related touchpoints
                    and (
                        -- FORMULA BELOW from https://docs.google.com/spreadsheets/d/1cauC2ZhvBH6cApiiZoWWEi5-2G54PeS6NmcQCugdMvg/edit#gid=1467510128
                        (		
                        REGEXP_CONTAINS(lower(attributes), r'[^a-z](bws)[^a-z]')		
                        or REGEXP_CONTAINS(lower(attributes), r'[^a-z](liquor)[^a-z]')		
                        or REGEXP_CONTAINS(lower(attributes), r'[^a-z](alcohol)[^a-z]')		
                        or REGEXP_CONTAINS(lower(attributes), r'[^a-z](beer)[^a-z]')		
                        or REGEXP_CONTAINS(lower(attributes), r'[^a-z](wine)[^a-z]')		
                        or REGEXP_CONTAINS(lower(attributes), r'[^a-z](spirits)[^a-z]')		
                        or REGEXP_CONTAINS(lower(attributes), r'[^a-z](beer)[^a-z]')		
                        or REGEXP_CONTAINS(lower(attributes), r'[^a-z](craft beer)[^a-z]')	or REGEXP_CONTAINS(lower(attributes), r'[^a-z](craft%20beer)[^a-z]')	or REGEXP_CONTAINS(lower(attributes), r'[^a-z](craft-beer)[^a-z]')
                        or REGEXP_CONTAINS(lower(attributes), r'[^a-z](imported beer)[^a-z]')	or REGEXP_CONTAINS(lower(attributes), r'[^a-z](imported%20beer)[^a-z]')	or REGEXP_CONTAINS(lower(attributes), r'[^a-z](imported-beer)[^a-z]')
                        or REGEXP_CONTAINS(lower(attributes), r'[^a-z](australian beer)[^a-z]')	or REGEXP_CONTAINS(lower(attributes), r'[^a-z](australian%20beer)[^a-z]')	or REGEXP_CONTAINS(lower(attributes), r'[^a-z](australian-beer)[^a-z]')
                        or REGEXP_CONTAINS(lower(attributes), r'[^a-z](full strength beer)[^a-z]')	or REGEXP_CONTAINS(lower(attributes), r'[^a-z](full%20strength%20beer)[^a-z]')	or REGEXP_CONTAINS(lower(attributes), r'[^a-z](full%20strength%-beer)[^a-z]')
                        or REGEXP_CONTAINS(lower(attributes), r'[^a-z](mid strength beer)[^a-z]')	or REGEXP_CONTAINS(lower(attributes), r'[^a-z](mid%20strength%20beer)[^a-z]')	or REGEXP_CONTAINS(lower(attributes), r'[^a-z](mid%20strength%-beer)[^a-z]')
                        or REGEXP_CONTAINS(lower(attributes), r'[^a-z](light beer)[^a-z]')	or REGEXP_CONTAINS(lower(attributes), r'[^a-z](light%20beer)[^a-z]')	or REGEXP_CONTAINS(lower(attributes), r'[^a-z](light-beer)[^a-z]')
                        or REGEXP_CONTAINS(lower(attributes), r'[^a-z](low carb beer)[^a-z]')	or REGEXP_CONTAINS(lower(attributes), r'[^a-z](low%20carb%20beer)[^a-z]')	or REGEXP_CONTAINS(lower(attributes), r'[^a-z](low%20carb%-beer)[^a-z]')
                        or REGEXP_CONTAINS(lower(attributes), r'[^a-z](cider)[^a-z]')		
                        or REGEXP_CONTAINS(lower(attributes), r'[^a-z](lager)[^a-z]')		
                        or REGEXP_CONTAINS(lower(attributes), r'[^a-z](pale ale)[^a-z]')	or REGEXP_CONTAINS(lower(attributes), r'[^a-z](pale%20ale)[^a-z]')	or REGEXP_CONTAINS(lower(attributes), r'[^a-z](pale-ale)[^a-z]')
                        or REGEXP_CONTAINS(lower(attributes), r'[^a-z](indian pale ale)[^a-z]')	or REGEXP_CONTAINS(lower(attributes), r'[^a-z](indian%20pale%20ale)[^a-z]')	or REGEXP_CONTAINS(lower(attributes), r'[^a-z](indian%20pale%-ale)[^a-z]')
                        or REGEXP_CONTAINS(lower(attributes), r'[^a-z](pilsner)[^a-z]')		
                        or REGEXP_CONTAINS(lower(attributes), r'[^a-z](amber ale)[^a-z]')	or REGEXP_CONTAINS(lower(attributes), r'[^a-z](amber%20ale)[^a-z]')	or REGEXP_CONTAINS(lower(attributes), r'[^a-z](amber-ale)[^a-z]')
                        or REGEXP_CONTAINS(lower(attributes), r'[^a-z](red ale)[^a-z]')	or REGEXP_CONTAINS(lower(attributes), r'[^a-z](red%20ale)[^a-z]')	or REGEXP_CONTAINS(lower(attributes), r'[^a-z](red-ale)[^a-z]')
                        or REGEXP_CONTAINS(lower(attributes), r'[^a-z](wheat beer)[^a-z]')	or REGEXP_CONTAINS(lower(attributes), r'[^a-z](wheat%20beer)[^a-z]')	or REGEXP_CONTAINS(lower(attributes), r'[^a-z](wheat-beer)[^a-z]')
                                
                        or REGEXP_CONTAINS(lower(attributes), r'[^a-z](ale)[^a-z]')		
                        or REGEXP_CONTAINS(lower(attributes), r'[^a-z](corona)[^a-z]')		
                        or REGEXP_CONTAINS(lower(attributes), r'[^a-z](victoria bitter)[^a-z]')	or REGEXP_CONTAINS(lower(attributes), r'[^a-z](victoria%20bitter)[^a-z]')	or REGEXP_CONTAINS(lower(attributes), r'[^a-z](victoria-bitter)[^a-z]')
                        or REGEXP_CONTAINS(lower(attributes), r'[^a-z](xxxx)[^a-z]')		
                        or REGEXP_CONTAINS(lower(attributes), r'[^a-z](carlton)[^a-z]')		
                        or REGEXP_CONTAINS(lower(attributes), r'[^a-z](coopers)[^a-z]')		
                                
                        or REGEXP_CONTAINS(lower(attributes), r'[^a-z](hahn)[^a-z]')		
                        or REGEXP_CONTAINS(lower(attributes), r'[^a-z](somersby)[^a-z]')		
                        or REGEXP_CONTAINS(lower(attributes), r'[^a-z](red wine)[^a-z]')	or REGEXP_CONTAINS(lower(attributes), r'[^a-z](red%20wine)[^a-z]')	or REGEXP_CONTAINS(lower(attributes), r'[^a-z](red-wine)[^a-z]')
                        or REGEXP_CONTAINS(lower(attributes), r'[^a-z](shiraz)[^a-z]')		
                        or REGEXP_CONTAINS(lower(attributes), r'[^a-z](cabernet sauvignon)[^a-z]')	or REGEXP_CONTAINS(lower(attributes), r'[^a-z](cabernet%20sauvignon)[^a-z]')	or REGEXP_CONTAINS(lower(attributes), r'[^a-z](cabernet-sauvignon)[^a-z]')
                        or REGEXP_CONTAINS(lower(attributes), r'[^a-z](merlot)[^a-z]')		
                        or REGEXP_CONTAINS(lower(attributes), r'[^a-z](pinot noir)[^a-z]')	or REGEXP_CONTAINS(lower(attributes), r'[^a-z](pinot%20noir)[^a-z]')	or REGEXP_CONTAINS(lower(attributes), r'[^a-z](pinot-noir)[^a-z]')
                        or REGEXP_CONTAINS(lower(attributes), r'[^a-z](rosé)[^a-z]')		
                        or REGEXP_CONTAINS(lower(attributes), r'[^a-z](grenache)[^a-z]')		
                        or REGEXP_CONTAINS(lower(attributes), r'[^a-z](tempranillo)[^a-z]')		
                        or REGEXP_CONTAINS(lower(attributes), r'[^a-z](sangiovese)[^a-z]')		
                        or REGEXP_CONTAINS(lower(attributes), r'[^a-z](white wine)[^a-z]')	or REGEXP_CONTAINS(lower(attributes), r'[^a-z](white%20wine)[^a-z]')	or REGEXP_CONTAINS(lower(attributes), r'[^a-z](white-wine)[^a-z]')
                        or REGEXP_CONTAINS(lower(attributes), r'[^a-z](chardonnay)[^a-z]')		
                        or REGEXP_CONTAINS(lower(attributes), r'[^a-z](sauvignon blanc)[^a-z]')	or REGEXP_CONTAINS(lower(attributes), r'[^a-z](sauvignon%20blanc)[^a-z]')	or REGEXP_CONTAINS(lower(attributes), r'[^a-z](sauvignon-blanc)[^a-z]')
                        or REGEXP_CONTAINS(lower(attributes), r'[^a-z](riesling)[^a-z]')		
                        or REGEXP_CONTAINS(lower(attributes), r'[^a-z](pinot ris)[^a-z]')	or REGEXP_CONTAINS(lower(attributes), r'[^a-z](pinot%20ris)[^a-z]')	or REGEXP_CONTAINS(lower(attributes), r'[^a-z](pinot-ris)[^a-z]')
                        or REGEXP_CONTAINS(lower(attributes), r'[^a-z](grigio)[^a-z]')		
                        or REGEXP_CONTAINS(lower(attributes), r'[^a-z](semillon)[^a-z]')		
                        or REGEXP_CONTAINS(lower(attributes), r'[^a-z](semillon sauvignon blanc)[^a-z]')	or REGEXP_CONTAINS(lower(attributes), r'[^a-z](semillon%20sauvignon%20blanc)[^a-z]')	or REGEXP_CONTAINS(lower(attributes), r'[^a-z](semillon%20sauvignon%-blanc)[^a-z]')
                        or REGEXP_CONTAINS(lower(attributes), r'[^a-z](white blends)[^a-z]')	or REGEXP_CONTAINS(lower(attributes), r'[^a-z](white%20blends)[^a-z]')	or REGEXP_CONTAINS(lower(attributes), r'[^a-z](white-blends)[^a-z]')
                        or REGEXP_CONTAINS(lower(attributes), r'[^a-z](moscato)[^a-z]')		
                        or REGEXP_CONTAINS(lower(attributes), r'[^a-z](sweet wine)[^a-z]')	or REGEXP_CONTAINS(lower(attributes), r'[^a-z](sweet%20wine)[^a-z]')	or REGEXP_CONTAINS(lower(attributes), r'[^a-z](sweet-wine)[^a-z]')
                        or REGEXP_CONTAINS(lower(attributes), r'[^a-z](prosecco)[^a-z]')		
                        or REGEXP_CONTAINS(lower(attributes), r'[^a-z](champagne)[^a-z]')		
                        or REGEXP_CONTAINS(lower(attributes), r'[^a-z](sparkling whites)[^a-z]')	or REGEXP_CONTAINS(lower(attributes), r'[^a-z](sparkling%20whites)[^a-z]')	or REGEXP_CONTAINS(lower(attributes), r'[^a-z](sparkling-whites)[^a-z]')
                        or REGEXP_CONTAINS(lower(attributes), r'[^a-z](sparkling reds)[^a-z]')	or REGEXP_CONTAINS(lower(attributes), r'[^a-z](sparkling%20reds)[^a-z]')	or REGEXP_CONTAINS(lower(attributes), r'[^a-z](sparkling-reds)[^a-z]')
                        or REGEXP_CONTAINS(lower(attributes), r'[^a-z](sparkling rosé)[^a-z]')	or REGEXP_CONTAINS(lower(attributes), r'[^a-z](sparkling%20rosé)[^a-z]')	or REGEXP_CONTAINS(lower(attributes), r'[^a-z](sparkling-rosé)[^a-z]')
                        or REGEXP_CONTAINS(lower(attributes), r'[^a-z](sweet sparkling)[^a-z]')	or REGEXP_CONTAINS(lower(attributes), r'[^a-z](sweet%20sparkling)[^a-z]')	or REGEXP_CONTAINS(lower(attributes), r'[^a-z](sweet-sparkling)[^a-z]')
                        or REGEXP_CONTAINS(lower(attributes), r'[^a-z](sparkling wine)[^a-z]')	or REGEXP_CONTAINS(lower(attributes), r'[^a-z](sparkling%20wine)[^a-z]')	or REGEXP_CONTAINS(lower(attributes), r'[^a-z](sparkling-wine)[^a-z]')
                        or REGEXP_CONTAINS(lower(attributes), r'[^a-z](spirits)[^a-z]')		
                        or REGEXP_CONTAINS(lower(attributes), r'[^a-z](whisky)[^a-z]')		
                        or REGEXP_CONTAINS(lower(attributes), r'[^a-z](bourbon)[^a-z]')		
                        or REGEXP_CONTAINS(lower(attributes), r'[^a-z](rum)[^a-z]')		
                        or REGEXP_CONTAINS(lower(attributes), r'[^a-z](vodka)[^a-z]')		
                        or REGEXP_CONTAINS(lower(attributes), r'[^a-z](gin)[^a-z]')		
                        or REGEXP_CONTAINS(lower(attributes), r'[^a-z](tequila)[^a-z]')		
                        or REGEXP_CONTAINS(lower(attributes), r'[^a-z](liqueurs)[^a-z]')		
                        or REGEXP_CONTAINS(lower(attributes), r'[^a-z](brandy)[^a-z]')		
                        or REGEXP_CONTAINS(lower(attributes), r'[^a-z](cognac)[^a-z]')		
                        or REGEXP_CONTAINS(lower(attributes), r'[^a-z](premium whisky)[^a-z]')	or REGEXP_CONTAINS(lower(attributes), r'[^a-z](premium%20whisky)[^a-z]')	or REGEXP_CONTAINS(lower(attributes), r'[^a-z](premium-whisky)[^a-z]')
                        or REGEXP_CONTAINS(lower(attributes), r'[^a-z](premium bourbon)[^a-z]')	or REGEXP_CONTAINS(lower(attributes), r'[^a-z](premium%20bourbon)[^a-z]')	or REGEXP_CONTAINS(lower(attributes), r'[^a-z](premium-bourbon)[^a-z]')
                        or REGEXP_CONTAINS(lower(attributes), r'[^a-z](premium vodka)[^a-z]')	or REGEXP_CONTAINS(lower(attributes), r'[^a-z](premium%20vodka)[^a-z]')	or REGEXP_CONTAINS(lower(attributes), r'[^a-z](premium-vodka)[^a-z]')
                        or REGEXP_CONTAINS(lower(attributes), r'[^a-z](premium gin)[^a-z]')	or REGEXP_CONTAINS(lower(attributes), r'[^a-z](premium%20gin)[^a-z]')	or REGEXP_CONTAINS(lower(attributes), r'[^a-z](premium-gin)[^a-z]')
                                
                        or REGEXP_CONTAINS(lower(attributes), r'[^a-z](johnnie walker)[^a-z]')	or REGEXP_CONTAINS(lower(attributes), r'[^a-z](johnnie%20walker)[^a-z]')	or REGEXP_CONTAINS(lower(attributes), r'[^a-z](johnnie-walker)[^a-z]')
                        or REGEXP_CONTAINS(lower(attributes), r'[^a-z](jim beam)[^a-z]')	or REGEXP_CONTAINS(lower(attributes), r'[^a-z](jim%20beam)[^a-z]')	or REGEXP_CONTAINS(lower(attributes), r'[^a-z](jim-beam)[^a-z]')
                        or REGEXP_CONTAINS(lower(attributes), r'[^a-z](jack daniels)[^a-z]')	or REGEXP_CONTAINS(lower(attributes), r'[^a-z](jack%20daniels)[^a-z]')	or REGEXP_CONTAINS(lower(attributes), r'[^a-z](jack-daniels)[^a-z]')
                        or REGEXP_CONTAINS(lower(attributes), r'[^a-z](canadian club)[^a-z]')	or REGEXP_CONTAINS(lower(attributes), r'[^a-z](canadian%20club)[^a-z]')	or REGEXP_CONTAINS(lower(attributes), r'[^a-z](canadian-club)[^a-z]')
                                
                        or REGEXP_CONTAINS(lower(attributes), r'[^a-z](smirnoff)[^a-z]')		
                        or REGEXP_CONTAINS(lower(attributes), r'[^a-z](wild turkey)[^a-z]')	or REGEXP_CONTAINS(lower(attributes), r'[^a-z](wild%20turkey)[^a-z]')	or REGEXP_CONTAINS(lower(attributes), r'[^a-z](wild-turkey)[^a-z]')
                                
                        ) AND NOT (		
                        REGEXP_CONTAINS(lower(attributes), r'[^a-z](liquorice)[^a-z]')		
                        or REGEXP_CONTAINS(lower(attributes), r'[^a-z](ginger)[^a-z]')		
                        or REGEXP_CONTAINS(lower(attributes), r'[^a-z](cooking wine)[^a-z]')	or REGEXP_CONTAINS(lower(attributes), r'[^a-z](cooking%20wine)[^a-z]')	or REGEXP_CONTAINS(lower(attributes), r'[^a-z](cooking-wine)[^a-z]')
                        or REGEXP_CONTAINS(lower(attributes), r'[^a-z](maggie beer)[^a-z]')	or REGEXP_CONTAINS(lower(attributes), r'[^a-z](maggie%20beer)[^a-z]')	or REGEXP_CONTAINS(lower(attributes), r'[^a-z](maggie-beer)[^a-z]')
                        or REGEXP_CONTAINS(lower(attributes), r'[^a-z](vinegar)[^a-z]')		
                        or REGEXP_CONTAINS(lower(attributes), r'[^a-z](beerenberg)[^a-z]')		
                        or REGEXP_CONTAINS(lower(attributes), r'[^a-z](wedges)[^a-z]')		
                        or REGEXP_CONTAINS(lower(attributes), r'[^a-z](chips)[^a-z]')		
                        or REGEXP_CONTAINS(lower(attributes), r'[^a-z](batter)[^a-z]')		
                        or REGEXP_CONTAINS(lower(attributes), r'[^a-z](cream)[^a-z]')		
                        or REGEXP_CONTAINS(lower(attributes), r'[^a-z](custard)[^a-z]')		
                        )
                    )
            )
        )
        where event_rank = 1    
    );

    --------------------------------------------------
    -- BWS TOUCHPOINTS ALL
    --------------------------------------------------
    -- combine wilson's email opens with event store touchpoints
    CREATE OR REPLACE TABLE `wx-bq-poc.personal.AL_tmp_bws_touchpoints_{fw_no_dash}` AS (
    with tmp as (
        select
        crn,
        CASE WHEN campaign_code is null then '_AlwaysOn' else campaign_code end as campaign_code,
        channel,
        event,
        -- event store seems to be 12 hours behind Wilson's data
        DATE_ADD(time_utc, INTERVAL 12 HOUR) as time_utc,
        "eventstore" as source
        FROM
        `wx-bq-poc.digital_attribution_modelling.dacamp_prod_eventstore_view_snapshot`
        ######################################################################
        # REMEMBER TO SWITCH BACK TO REAL EVENTSTORE
        ######################################################################
        # `wx-bq-poc.personal.AL_dacamp_prod_eventstore_view_snapshot_20210830`
        WHERE banner = 'bws'

        UNION ALL

        SELECT
        crn,
        campaign_code,
        "email" as channel,
        "open" as event,
        ts as time_utc,
        "safari events" as source
        FROM `wx-bq-poc.personal.AL_tmp_bws_email_open_{fw_no_dash}`

        UNION ALL

        SELECT
        crn,
        campaign_code,
        channel,
        event,
        -- event store seems to be 12 hours behind Wilson's data
        DATE_ADD(time_utc, INTERVAL 12 HOUR) as time_utc,
        "eventstore" as source
        FROM `wx-bq-poc.personal.AL_tmp_bws_wow_web_{fw_no_dash}`
    )
    select * from tmp
    -- order by crn, campaign_code, channel, event, time_utc   
    )
    ;

    -- dedup to get rid of identical email touchpoints from Wilson's table and Eventstore
    CREATE OR REPLACE TABLE `wx-bq-poc.personal.AL_tmp_bws_touchpoints_dedup_{fw_no_dash}` AS (
    select
    *
    from (
    SELECT
        *,
        ROW_NUMBER()
            OVER (PARTITION BY crn, campaign_code,channel, event, time_utc order by crn, campaign_code,channel, event, time_utc, source)
            row_number
    FROM
        `wx-bq-poc.personal.AL_tmp_bws_touchpoints_{fw_no_dash}`  
    )
    WHERE row_number = 1
    )
    ;

    --------------------------------------------------
    -- JOIN TOUCHPOINTS ONTO OPENS AND ACTIVATIONS
    --------------------------------------------------
    -- sometimes the CRN is not in the event store at all, despite being in Wilson's data
    -- sometimes the CRN activates without any email sent/opens in Wilson's data
    CREATE OR REPLACE TABLE
    `wx-bq-poc.digital_attribution_modelling.bws_mta_events_{fw_no_dash}` AS (
    select 
        a.crn,
        a.campaign_code,
        a.campaign_start_date,
        a.campaign_end_date,
        a.time_utc as conv_time,
        coalesce(b.channel, c.channel, d.channel) as channel,
        coalesce(b.event, c.event, d.event) as event_touchpoint,
        coalesce(b.time_utc, c.time_utc, d.time_utc) as time_utc,
        coalesce(concat(b.channel, "_", b.event), concat(c.channel, "_", c.event), concat(d.campaign_code, "_", d.channel, "_", d.event)) as event_node,

        case when b.crn is not null then 1 else 0 end as touchpoint_from_eventstore,
        case when c.crn is not null then 1 else 0 end as touchpoint_from_safari,
        case when b.crn is not null or c.crn is not null then 1 else 0 end as touchpoint_exists
    from
        -- `wx-bq-poc.digital_attribution_modelling.bws_activations_opens_{fw_no_dash}` as a
        `wx-bq-poc.digital_attribution_modelling.bws_mta_activations_{fw_no_dash}` as a

    -- join on event store touchpoints
    left join `wx-bq-poc.personal.AL_tmp_bws_touchpoints_dedup_{fw_no_dash}` as b
        on
        b.source = "eventstore" and
        a.crn = b.crn and
        a.campaign_code = b.campaign_code and
        cast(b.time_utc as date) >= date_sub(a.campaign_start_date, INTERVAL 3 DAY) and (cast(b.time_utc as date) <= date_add(a.campaign_end_date, INTERVAL 3 DAY) or a.campaign_end_date is null)  -- null indicates the campaign is still ongoing
    -- join on safari events when event store has no touchpoints
    left join `wx-bq-poc.personal.AL_tmp_bws_touchpoints_dedup_{fw_no_dash}` as c
        on
        c.source = "safari events" and
        -- only join emails from Wilson's table when there are no event store touchpoints
        b.crn is null and
        a.crn = c.crn and
        a.campaign_code = c.campaign_code and
        cast(c.time_utc as date) >= date_sub(a.campaign_start_date, INTERVAL 3 DAY) and (cast(c.time_utc as date) <= date_add(a.campaign_end_date, INTERVAL 3 DAY) or a.campaign_end_date is null)  -- null indicates the campaign is still ongoing
    -- join extracted always on touchpoints
    left join `wx-bq-poc.personal.AL_tmp_bws_wow_web_always_on_{fw_no_dash}` as d
        on
        a.crn = d.crn
        and
        cast(d.time_utc as date) >= date_sub(a.campaign_start_date, INTERVAL 3 DAY) and (cast(d.time_utc as date) <= date_add(a.campaign_end_date, INTERVAL 3 DAY) or a.campaign_end_date is null)  -- null indicates the campaign is still ongoing
    )
    ;

    -- -- random sample to check
    -- SELECT
    -- *,
    -- RAND() as x
    -- FROM `wx-bq-poc.digital_attribution_modelling.bws_mta_events_{fw_no_dash}`
    -- ORDER BY x
    -- LIMIT 1000
    -- ;


    -- SELECT
    -- channel,
    -- event_touchpoint,
    -- count(*)
    -- FROM `wx-bq-poc.digital_attribution_modelling.bws_mta_events_{fw_no_dash}`
    -- group by 1,2
    -- order by 1,2
    -- ;

    -- check % of CRNs with at least 1 touchpoint in the event store
    INSERT INTO `wx-bq-poc.digital_attribution_modelling.bws_mta_checks_{fw_no_dash}`(
    -- create or replace table `wx-bq-poc.digital_attribution_modelling.bws_mta_checks_{fw_no_dash}` as (
    (with tmp as (
        select
        crn,
        campaign_code,
        campaign_start_date,
        sum(touchpoint_from_eventstore) as num_touchpoints
        from
        `wx-bq-poc.digital_attribution_modelling.bws_mta_events_{fw_no_dash}`
        group by 1,2,3
    )
    , tmp2 as (
        select
        crn,
        campaign_code,
        campaign_start_date,
        case when num_touchpoints > 0 then 1 else 0 end as touchpoints_exist
        from tmp  
    )
    select
        3 as check_number,
        "Proportion of CRNs with at least 1 touchpoint in the event store" as check,
        avg(touchpoints_exist) as check_value,
        1 as ideal_check_value
    from tmp2)  
    );

    # ASSERT
    # (
    # (select check_value from `wx-bq-poc.digital_attribution_modelling.bws_mta_checks_{fw_no_dash}` where check_number = 3) > 0.4
    # ) AS 'ERROR: <40% of crn, campaign codes, campaign start dates have at least 1 touchpoint in the event store';

    -- check % of CRNs with at least 1 touchpoint from event store or safari inputs (email opens)
    INSERT INTO `wx-bq-poc.digital_attribution_modelling.bws_mta_checks_{fw_no_dash}`(
    with tmp as (
        select
            crn,
            campaign_code,
            campaign_start_date,
            sum(touchpoint_exists) as num_touchpoints
        from
            `wx-bq-poc.digital_attribution_modelling.bws_mta_events_{fw_no_dash}`
        group by 1,2,3
        )
        , tmp2 as (
        select
            crn,
            campaign_code,
            campaign_start_date,
            case when num_touchpoints > 0 then 1 else 0 end as touchpoints_exist
        from tmp  
        )
        select
        4 as check_number,
        "Proportion of CRNs with at least 1 touchpoint from event store or safari inputs (email opens)" as check,
        avg(touchpoints_exist) as check_value,
        1 as ideal_check_value
        from tmp2
    );

    # ASSERT
    # (
    # (select check_value from `wx-bq-poc.digital_attribution_modelling.bws_mta_checks_{fw_no_dash}` where check_number = 4) > 0.5
    # ) AS 'ERROR: <50% of crn, campaign codes, campaign start dates have at least 1 touchpoint in the event store/safari inputs';

    --------------------------------------------------
    -- CREATING PATHS
    --------------------------------------------------
    -- running subtotal for pivoting
    -- each CRN will have many rows, one for each event node
    CREATE OR REPLACE TABLE `wx-bq-poc.personal.AL_bws_mta_cj_1_{fw_no_dash}` AS
    SELECT
    crn,
    campaign_code,
    campaign_start_date,

    event_node,
    time_utc ,
    concat("event_",
    COUNT(*) OVER (PARTITION BY crn, campaign_code, campaign_start_date
        ORDER BY
        crn,
        campaign_code,
        campaign_start_date,

        time_utc
        -- need tie breakers for when events happen at the same time?
        
        ROWS BETWEEN UNBOUNDED PRECEDING
        AND CURRENT ROW )) AS event_number_this_wk
    FROM
    `wx-bq-poc.digital_attribution_modelling.bws_mta_events_{fw_no_dash}`
    ORDER BY
        crn,
        campaign_code,
        campaign_start_date,
        time_utc

    ;

    -- transposing rows to columns
    CREATE OR REPLACE TABLE `wx-bq-poc.personal.AL_bws_mta_cj_2_{fw_no_dash}` AS
    SELECT 
    crn,
    campaign_code,
    campaign_start_date,
        MAX(IF(event_number_this_wk = 'event_1' , concat( '' ,event_node), '')) AS event_1,
        MAX(IF(event_number_this_wk = 'event_2' , concat( ' > ' ,event_node), '')) AS event_2,
        MAX(IF(event_number_this_wk = 'event_3' , concat( ' > ' ,event_node), '')) AS event_3,
        MAX(IF(event_number_this_wk = 'event_4' , concat( ' > ' ,event_node), '')) AS event_4,
        MAX(IF(event_number_this_wk = 'event_5' , concat( ' > ' ,event_node), '')) AS event_5,
        MAX(IF(event_number_this_wk = 'event_6' , concat( ' > ' ,event_node), '')) AS event_6,
        MAX(IF(event_number_this_wk = 'event_7' , concat( ' > ' ,event_node), '')) AS event_7,
        MAX(IF(event_number_this_wk = 'event_8' , concat( ' > ' ,event_node), '')) AS event_8,
        MAX(IF(event_number_this_wk = 'event_9' , concat( ' > ' ,event_node), '')) AS event_9,
        MAX(IF(event_number_this_wk = 'event_10' , concat( ' > ' ,event_node), '')) AS event_10,
        MAX(IF(event_number_this_wk = 'event_11' , concat( ' > ' ,event_node), '')) AS event_11,
        MAX(IF(event_number_this_wk = 'event_12' , concat( ' > ' ,event_node), '')) AS event_12,
        MAX(IF(event_number_this_wk = 'event_13' , concat( ' > ' ,event_node), '')) AS event_13,
        MAX(IF(event_number_this_wk = 'event_14' , concat( ' > ' ,event_node), '')) AS event_14,
        MAX(IF(event_number_this_wk = 'event_15' , concat( ' > ' ,event_node), '')) AS event_15,
        MAX(IF(event_number_this_wk = 'event_16' , concat( ' > ' ,event_node), '')) AS event_16,
        MAX(IF(event_number_this_wk = 'event_17' , concat( ' > ' ,event_node), '')) AS event_17,
        MAX(IF(event_number_this_wk = 'event_18' , concat( ' > ' ,event_node), '')) AS event_18,
        MAX(IF(event_number_this_wk = 'event_19' , concat( ' > ' ,event_node), '')) AS event_19,
        MAX(IF(event_number_this_wk = 'event_20' , concat( ' > ' ,event_node), '')) AS event_20,
        MAX(IF(event_number_this_wk = 'event_21' , concat( ' > ' ,event_node), '')) AS event_21,
        MAX(IF(event_number_this_wk = 'event_22' , concat( ' > ' ,event_node), '')) AS event_22,
        MAX(IF(event_number_this_wk = 'event_23' , concat( ' > ' ,event_node), '')) AS event_23,
        MAX(IF(event_number_this_wk = 'event_24' , concat( ' > ' ,event_node), '')) AS event_24,
        MAX(IF(event_number_this_wk = 'event_25' , concat( ' > ' ,event_node), '')) AS event_25,
        MAX(IF(event_number_this_wk = 'event_26' , concat( ' > ' ,event_node), '')) AS event_26,
        MAX(IF(event_number_this_wk = 'event_27' , concat( ' > ' ,event_node), '')) AS event_27,
        MAX(IF(event_number_this_wk = 'event_28' , concat( ' > ' ,event_node), '')) AS event_28,
        MAX(IF(event_number_this_wk = 'event_29' , concat( ' > ' ,event_node), '')) AS event_29,
        MAX(IF(event_number_this_wk = 'event_30' , concat( ' > ' ,event_node), '')) AS event_30,
        MAX(IF(event_number_this_wk = 'event_31' , concat( ' > ' ,event_node), '')) AS event_31,
        MAX(IF(event_number_this_wk = 'event_32' , concat( ' > ' ,event_node), '')) AS event_32,
        MAX(IF(event_number_this_wk = 'event_33' , concat( ' > ' ,event_node), '')) AS event_33,
        MAX(IF(event_number_this_wk = 'event_34' , concat( ' > ' ,event_node), '')) AS event_34,
        MAX(IF(event_number_this_wk = 'event_35' , concat( ' > ' ,event_node), '')) AS event_35,
        MAX(IF(event_number_this_wk = 'event_36' , concat( ' > ' ,event_node), '')) AS event_36,
        MAX(IF(event_number_this_wk = 'event_37' , concat( ' > ' ,event_node), '')) AS event_37,
        MAX(IF(event_number_this_wk = 'event_38' , concat( ' > ' ,event_node), '')) AS event_38,
        MAX(IF(event_number_this_wk = 'event_39' , concat( ' > ' ,event_node), '')) AS event_39,
        MAX(IF(event_number_this_wk = 'event_40' , concat( ' > ' ,event_node), '')) AS event_40,
        MAX(IF(event_number_this_wk = 'event_41' , concat( ' > ' ,event_node), '')) AS event_41,
        MAX(IF(event_number_this_wk = 'event_42' , concat( ' > ' ,event_node), '')) AS event_42,
        MAX(IF(event_number_this_wk = 'event_43' , concat( ' > ' ,event_node), '')) AS event_43,
        MAX(IF(event_number_this_wk = 'event_44' , concat( ' > ' ,event_node), '')) AS event_44,
        MAX(IF(event_number_this_wk = 'event_45' , concat( ' > ' ,event_node), '')) AS event_45,
        MAX(IF(event_number_this_wk = 'event_46' , concat( ' > ' ,event_node), '')) AS event_46,
        MAX(IF(event_number_this_wk = 'event_47' , concat( ' > ' ,event_node), '')) AS event_47,
        MAX(IF(event_number_this_wk = 'event_48' , concat( ' > ' ,event_node), '')) AS event_48,
        MAX(IF(event_number_this_wk = 'event_49' , concat( ' > ' ,event_node), '')) AS event_49,
        MAX(IF(event_number_this_wk = 'event_50' , concat( ' > ' ,event_node), '')) AS event_50 
    FROM `wx-bq-poc.personal.AL_bws_mta_cj_1_{fw_no_dash}`
    GROUP BY crn, campaign_code, campaign_start_date
    ORDER BY crn,   campaign_code, campaign_start_date;

    -- creating path
    CREATE OR REPLACE TABLE
    `wx-bq-poc.personal.AL_bws_mta_cj_3_{fw_no_dash}` AS
    SELECT 
    crn,
    campaign_code,
    campaign_start_date,
    concat(
        event_1,
        event_2,
        event_3,
        event_4,
        event_5,
        event_6,
        event_7,
        event_8,
        event_9,
        event_10,
        event_11,
        event_12,
        event_13,
        event_14,
        event_15,
        event_16,
        event_17,
        event_18,
        event_19,
        event_20,
        event_21,
        event_22,
        event_23,
        event_24,
        event_25,
        event_26,
        event_27,
        event_28,
        event_29,
        event_30,
        event_31,
        event_32,
        event_33,
        event_34,
        event_35,
        event_36,
        event_37,
        event_38,
        event_39,
        event_40,
        event_41,
        event_42,
        event_43,
        event_44,
        event_45,
        event_46,
        event_47,
        event_48,
        event_49,
        event_50
    ) as path
    
    FROM `wx-bq-poc.personal.AL_bws_mta_cj_2_{fw_no_dash}`
    ORDER BY   crn,
    campaign_code,
    campaign_start_date;

    --------------------------------------------------
    -- JOIN ON INC SALES
    -------------------------------------------------
    create or replace table `wx-bq-poc.personal.AL_bws_mta_cj_4_{fw_no_dash}` as (
    with inc_sales_by_crn as (
    select crn, campaign_code, campaign_start_date, sum(spend) as inc_sales
    from `wx-bq-poc.digital_attribution_modelling.bws_mta_activations_{fw_no_dash}`
    group by 1,2,3
    order by 1,2,3
    )
    select
    a.crn,
    a.campaign_code,
    a.campaign_start_date,
    a.path,
    b.inc_sales
    from
    `wx-bq-poc.personal.AL_bws_mta_cj_3_{fw_no_dash}` as a
    left join
    inc_sales_by_crn as b
    on
        a.crn = b.crn and
        a.campaign_code = b.campaign_code and
        a.campaign_start_date = b.campaign_start_date
    order by 1,2,3
    );

    create or replace table `wx-bq-poc.personal.AL_bws_mta_cj_5_{fw_no_dash}` as (
    select
        campaign_code,
        campaign_start_date,
        path,
        sum(inc_sales) as inc_sales,
        -- assume they all convert (business logic assumption)
        count(*) as conversions,
        0 as non_conversions
    from
        `wx-bq-poc.personal.AL_bws_mta_cj_4_{fw_no_dash}`
    group by 1,2,3
    order by 1,2,3
    );    

    INSERT INTO `wx-bq-poc.digital_attribution_modelling.bws_mta_checks_{fw_no_dash}`(
    with tmp as (
    select
        *,
        case when path is null then 0 else 1 end as has_path,
        case when inc_sales is null then 0 else 1 end as has_inc_sales
    from `wx-bq-poc.personal.AL_bws_mta_cj_5_{fw_no_dash}`
    )
    select
    5 as check_number,
    "Proportion rows with a non-null path" as check,
    avg(has_path) as check_value,
    1 as ideal_check_value
    from tmp
    union all
    select
    6 as check_number,
    "Proportion rows with a non-null inc sales" as check,
    avg(has_inc_sales) as check_value,
    1 as ideal_check_value
    from tmp
    );

    -- filter out the very small number of nulls
    create or replace table `wx-bq-poc.digital_attribution_modelling.bws_mta_customer_journeys_{fw_no_dash}` as (
    select
        *
    from
        `wx-bq-poc.personal.AL_bws_mta_cj_5_{fw_no_dash}`
    where inc_sales is not null and path is not null
    ); 

    -- deleting temp tables
    # drop table `wx-bq-poc.personal.AL_tmp_bws_email_open_{fw_no_dash}`;
    # drop table `wx-bq-poc.personal.AL_tmp_bws_wow_web_{fw_no_dash}`;
    # drop table `wx-bq-poc.personal.AL_tmp_bws_wow_web_always_on_{fw_no_dash}`;
    # drop table `wx-bq-poc.personal.AL_tmp_bws_touchpoints_{fw_no_dash}`;
    # drop table `wx-bq-poc.personal.AL_tmp_bws_touchpoints_dedup_{fw_no_dash}`;
    # drop table `wx-bq-poc.personal.AL_bws_mta_cj_1_{fw_no_dash}`;
    # drop table `wx-bq-poc.personal.AL_bws_mta_cj_2_{fw_no_dash}`;
    # drop table `wx-bq-poc.personal.AL_bws_mta_cj_3_{fw_no_dash}`;
    # drop table `wx-bq-poc.personal.AL_bws_mta_cj_4_{fw_no_dash}`;
    # drop table `wx-bq-poc.personal.AL_bws_mta_cj_5_{fw_no_dash}`;

    -- return checks
    select * from `wx-bq-poc.digital_attribution_modelling.bws_mta_checks_{fw_no_dash}`;

    """.format(fw=fw,
    fw_no_dash=fw_no_dash,
    dummy_data_comment=dummy_data_comment,
    dummy_data_extended_comment=dummy_data_extended_comment,
    real_data_comment=real_data_comment,
    input_data=input_data)

    df_bq = (
    bqclient.query(query_string)
    .result()
    .to_dataframe(bqstorage_client=bqstorageclient)
    )

    logging.info("ran create_customer_journeys for " + fw)
    logging.info("outputted to: `wx-bq-poc.digital_attribution_modelling.bws_mta_customer_journeys_" + fw_no_dash + "`")
    logging.info("see this for checks: `wx-bq-poc.digital_attribution_modelling.bws_mta_checks_" + fw_no_dash + "`")

    # log checks
    logging.info(str(df_bq))
    
    return df_bq

if __name__ == "__main__":
    ##################################################
    # LOGGING
    ##################################################
    # logging.basicConfig(level=logging.DEBUG)
    logFormatter = logging.Formatter("%(asctime)s [%(levelname)-7.7s]  %(message)s")
    rootLogger = logging.getLogger()
    rootLogger.setLevel(logging.INFO)  # need to set root logger to the lowest level
    
    # grab credentials from default login, use gcloud auth login
    credentials, your_project_id = google.auth.default(
        scopes=["https://www.googleapis.com/auth/cloud-platform"]
    )
    
    # Make clients.
    bqclient = bigquery.Client(credentials=credentials, project='wx-bq-poc',)
    bqstorageclient = bigquery_storage.BigQueryReadClient(credentials=credentials)
    FW = '2021-04-19'
    create_customer_journeys(fw=FW, use_dummy_data=False, bqclient=bqclient, bqstorageclient=bqstorageclient, rerun=True)
