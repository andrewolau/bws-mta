"""


"""
import logging

import google.auth
from google.cloud import bigquery
from google.cloud import bigquery_storage

def update_final_tables(fw, bqclient, bqstorageclient):
    """
    Overview
        BQ script https://console.cloud.google.com/bigquery?sq=564585695625:6f177df61b004ae98bdb65008e839b79
        
        Updates the final combined Supers + Big W + BWS MTA tables for Weixing's post-MTA process:
            `gcp-wow-rwds-ai-safari-prod.digital_attribution_modelling.dacamp_prod_mc_final_crn`
            `gcp-wow-rwds-ai-safari-prod.digital_attribution_modelling.dacamp_prod_event_mw`
        Using:
            `wx-bq-poc.digital_attribution_modelling.bws_mta_crn_output_{fw_no_dash}`
            `wx-bq-poc.digital_attribution_modelling.bws_mta_event_mw_{fw_no_dash}`

    Arguments
        fw (string) - eg '2021-04-19'
        bqclient - google.cloud.bigquery()
        bqstorageclientgoogle.cloud.bigquery_storage()
    """
    logging.info("running update_final_tables for " + fw)
    fw_no_dash = fw[:4] + fw[5:7] + fw[-2:]

    query_string = """
    -- check that we are updating the right week
    ASSERT (
        (select
            max(week)
        from
            `gcp-wow-rwds-ai-safari-prod.digital_attribution_modelling.dacamp_prod_mc_final_crn`)
        = '{fw}'
    ) AS 'FW mismatch for dacamp_prod_mc_final_crn and BWS data';

    -- update mc_final_crn table with BWS values
    delete from
        `gcp-wow-rwds-ai-safari-prod.digital_attribution_modelling.dacamp_prod_mc_final_crn`
    where
        banner = 'bws'
    ;

    insert into 
        `gcp-wow-rwds-ai-safari-prod.digital_attribution_modelling.dacamp_prod_mc_final_crn`
    (
        select
            cast(week as DATE) as week,	
            crn,
            segment_cvm,	
            segment_lifestage,	
            segment_marketable,	
            banner,	
            campaign_code,	
            campaign_type,	
            campaign_start_date,	
            campaign_end_date,	
            total_sales,	
            inc_sales,	
            channel_event,	
            channel_prob_norm,	
            channel,	
            medium,	
            event,	
            attributed_conversion,	
            attributed_total_sales,	
            attributed_inc_sales	
        from
            `wx-bq-poc.digital_attribution_modelling.bws_mta_crn_output_{fw_no_dash}`
    );

    -- update event_mw table with BWS values
    delete from
        `gcp-wow-rwds-ai-safari-prod.digital_attribution_modelling.dacamp_prod_event_mw`
    where
        banner = 'bws'
    ;

    insert into 
        `gcp-wow-rwds-ai-safari-prod.digital_attribution_modelling.dacamp_prod_event_mw`
    (
        select
            banner,
            campaign_code,	
            campaign_type,	
            campaign_start_date,	
            campaign_end_date,	
            crn,
            time_utc,	
            date,	
            channel_event,	
            cast("2999-12-31" as TIMESTAMP) as conv_time,	
            conv_flag,	
            spend	
        from
            `wx-bq-poc.digital_attribution_modelling.bws_mta_event_mw_{fw_no_dash}`
    );
    """.format(fw=fw, fw_no_dash=fw_no_dash)

    df_bq = (
    bqclient.query(query_string)
    .result()
    .to_dataframe(bqstorage_client=bqstorageclient)
    )

    logging.info("ran update_final_tables for " + fw)
    logging.info("outputted to: `gcp-wow-rwds-ai-safari-prod.digital_attribution_modelling.dacamp_prod_mc_final_crn`")
    logging.info("outputted to: `gcp-wow-rwds-ai-safari-prod.digital_attribution_modelling.dacamp_prod_event_mw`")
    
    return df_bq

if __name__ == "__main__":
    ##################################################
    # LOGGING
    ##################################################
    # logging.basicConfig(level=logging.DEBUG)
    logFormatter = logging.Formatter("%(asctime)s [%(levelname)-7.7s]  %(message)s")
    rootLogger = logging.getLogger()
    rootLogger.setLevel(logging.INFO)  # need to set root logger to the lowest level
    
    # grab credentials from default login, use gcloud auth login
    credentials, your_project_id = google.auth.default(
        scopes=["https://www.googleapis.com/auth/cloud-platform"]
    )
    
    # Make clients.
    bqclient = bigquery.Client(credentials=credentials, project='wx-bq-poc',)
    bqstorageclient = bigquery_storage.BigQueryReadClient(credentials=credentials)
    FW = '2021-04-19'
    update_final_tables(fw=FW, use_dummy_data=False)
