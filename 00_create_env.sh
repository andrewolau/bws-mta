#!/bin/bash
# create the environment in the directory for easier access
echo "INFO: creating bws-mta conda environment"
conda create -n bws-mta -y
# conda create -n bws-mta -y
echo "INFO: activating bws-mta conda env"
source activate bws-mta

# # forked repo with custom SQL scripts
# python -m pip install git+https://andrewolau@bitbucket.org/andrewolau/mta.git

# packages are not all from conda
# need to export conda and pip install separately... doesn't seem
# to be a way to cleanly freeze all the requirements using either
# conda or pip
# conda recommends installing conda and pip packages separately when
# recreating the environment

# echo "INFO: exporting conda requirements"
# conda list --explicit > requirements-conda.txt

pip install seaborn pandas-gbq ChannelAttribution fsspec gcsfs 'google-cloud-bigquery[bqstorage,pandas]' google-api-python-client google-auth-httplib2 google-auth-oauthlib google-auth-oauthlib 

echo "INFO: exporting pip requirements"
pip freeze > requirements-pip.txt
# pip freeze | egrep "seaborn|pandas|Channel|fsspec|gcsfs|google" > requirements-pip.txt