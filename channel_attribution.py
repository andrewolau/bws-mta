"""
Overview
    Runs BWS Channel Attribution
    BQ input data:
    `wx-bq-poc.digital_attribution_modelling.bws_mta_customer_journeys_YYYYMMDD`
    BQ output data:
    `wx-bq-poc.digital_attribution_modelling.bws_mta_attribution_YYYYMMDD`
Usage    
    import channel_attribution
    channel_attribution.run_mta(fw, bqclient, bqstorageclient, rerun=False)
Author
    Andrew Lau
"""
import pandas as pd
import os
import google.auth
from google.cloud import bigquery
from google.cloud import bigquery_storage
from google.cloud.exceptions import NotFound
import logging
import ChannelAttribution

import utils

def run_mta(fw, bqclient, bqstorageclient, rerun=False):
    """ Run MTA for a FW

    BQ input data:
    `wx-bq-poc.digital_attribution_modelling.bws_mta_customer_journeys_YYYYMMDD`
    BQ output data:
    `wx-bq-poc.digital_attribution_modelling.bws_mta_attribution_YYYYMMDD`

    Args:
        fw_no_dash (string): financial week with no hyphen/dash
        bqclient (bigquery.Client): big query client object
        bqstorageclient (bigquery_storage.BigQueryReadClient): big query storage client object
        
    Returns:
        [pandas DF]: DF with channel attribution for all campaign code/campaign start dates in a FW
    """
    fw_no_dash = fw[:4] + fw[5:7] + fw[-2:]

    # check if already ran
    if utils.check_if_BQ_table_exists("wx-bq-poc.digital_attribution_modelling.bws_mta_attribution_{}".format(fw_no_dash)):
        if rerun:
            logging.warning("rerunning run_mta")
        else:
            logging.warning("skipping run_mta")
            return

    # get CJ data
    query_string = """
    select
        *
    from
        `wx-bq-poc.digital_attribution_modelling.bws_mta_customer_journeys_{fw_no_dash}`
    ;    
    """.format(fw_no_dash=fw_no_dash)
    df = (
        bqclient.query(query_string)
        .result()
        .to_dataframe(bqstorage_client=bqstorageclient)
    )
    logging.info("df" + str(df.head()))

    # create dict of unique campaigns and campaign start dates for looping
    unique_campaigns = df['campaign_code'].unique()
    logging.info('unique_campaigns: ' + str(unique_campaigns))

    campaign_dict = dict()
    for campaign in unique_campaigns:
        campaign_start_dates = df.loc[df.campaign_code == campaign, 'campaign_start_date'].unique()
        campaign_start_dates_str = []
        for date in campaign_start_dates:
            campaign_start_dates_str.append(date)
        campaign_dict[campaign] = campaign_start_dates_str

    logging.info('campaign_dict: ' + str(campaign_dict))

    # initialising empty DF to store all attribution at the campaign code, campaign start date level
    attribution_all = pd.DataFrame()

    # looping across all campaign code and campaign start dates to run MTA
    for campaign_code in campaign_dict:
        for campaign_start_date in campaign_dict[campaign_code]:
            logging.info('Running MTA for campaign_code, campaign_start_date' + str(campaign_code) + str(campaign_start_date))
            df_campaign = df[(df.campaign_code == campaign_code) & (df.campaign_start_date == campaign_start_date)]
            logging.info('extracted df_campaign for campaign code, campaign_start_date subset: ' + str(df_campaign))
            
            logging.info('df_campaign.shape is: ' + str(df_campaign.shape))
            logging.info('running ChannelAttribution.markov_model')
            attribution = ChannelAttribution.markov_model(Data=df_campaign,
                                                var_path='path',
                                                var_conv='conversions',
                                                var_null='non_conversions',
                                                var_value='inc_sales',
                                                order=1                                         
                                                )
            logging.info('finished ChannelAttribution.markov_model')
            # attribution seems to bug out when total inc sales is negative...
            # just use unweighted conversions for these
            if attribution['total_conversion_value'].sum() <= 0:
                logging.info("total campaign attribution is <= 0, using unweighted attribution")
                attribution['normalised_conversion_value'] = attribution['total_conversions'] / attribution['total_conversions'].sum()
            else:
                attribution['normalised_conversion_value'] = attribution['total_conversion_value'] / attribution['total_conversion_value'].sum()

            attribution.insert(loc=0, column="campaign_start_date", value=campaign_start_date)
            attribution.insert(loc=0, column="campaign_code", value=campaign_code)

            # attribution = mta.ca_from_journey(cj_df=df_campaign,
            #                                     var_path='path',
            #                                     var_conv='conversions',
            #                                     var_null='non_conversions',
            #                                     var_value='inc_sales',
            #                                     order=1)

            logging.info("attribution results: " + str(attribution))

            attribution_all = attribution_all.append(attribution)


    logging.info("attribution_all results: " + str(attribution_all))
    attribution_all.to_gbq('digital_attribution_modelling.bws_mta_attribution_' + fw_no_dash, project_id='wx-bq-poc', if_exists='replace')
    logging.info("dumped attribution_all results to BQ:  `wx-bq-poc.digital_attribution_modelling.bws_mta_attribution_" + fw_no_dash + '`')

    return attribution_all

if __name__ == '__main__':
    ##################################################
    # LOGGING
    ##################################################
    # logging.basicConfig(level=logging.DEBUG)
    logFormatter = logging.Formatter("%(asctime)s [%(levelname)-7.7s]  %(message)s")
    rootLogger = logging.getLogger()
    rootLogger.setLevel(logging.INFO)  # need to set root logger to the lowest level

    # now = datetime.datetime.now() + datetime.timedelta(hours=10)  # convert to Sydney time
    # now = now.strftime("%d-%m-%Y_%H%M")
    # # fileHandler = logging.FileHandler("{0}/{1}.log".format("logs", now), 'w+')
    # # fileHandler.setLevel(logging.DEBUG)
    # # fileHandler.setFormatter(logFormatter)
    # # rootLogger.addHandler(fileHandler)

    # consoleHandler = logging.StreamHandler()
    # consoleHandler.setLevel(logging.INFO)
    # consoleHandler.setFormatter(logFormatter)
    # rootLogger.addHandler(consoleHandler)

    # # catch all exceptions in the log
    # def my_handler(type, value, tb):
    #     rootLogger.exception("Uncaught exception: {0}".format(str(value)))

    # # Install exception handler
    # sys.excepthook = my_handler

    ##################################################
    # SETTING UP GCP CREDENTIALS AND BQ CLIENTS
    ##################################################
    # set user credentials to create BQ client object
    os.environ['GOOGLE_APPLICATION_CREDENTIALS']="adc.json" 
    os.environ['GOOGLE_CLOUD_PROJECT']="wx-bq-poc" 

    # grab creentials from default login, use gcloud auth login
    credentials, your_project_id = google.auth.default(
        scopes=["https://www.googleapis.com/auth/cloud-platform"]
    )

    # make clients.
    bqclient = bigquery.Client(credentials=credentials, project='wx-bq-poc',)
    bqstorageclient = bigquery_storage.BigQueryReadClient(credentials=credentials)   

    ##################################################
    # MTA
    ##################################################
    FW_NO_DASH = '2020-05-18'
    run_mta(fw=FW_NO_DASH, bqclient=bqclient, bqstorageclient=bqstorageclient)
