#!/bin/bash
# https://conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html#building-identical-conda-environments
# may need to run this first: conda config --append channels conda-forge
echo "INFO: conda create -n bws-mta -y"
# conda create --prefix ./bws-mta --file requirements-conda.txt -y
conda create -n bws-mta -y
echo "INFO: source activate bws-mta"
source activate bws-mta
echo "INFO: installing pip packages"
pip install -r requirements-pip.txt