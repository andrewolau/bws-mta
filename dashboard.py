"""
Overview
    Creates customer journeys in the ChannelAttribution format for MTA
    Outputs customer journeys to `wx-bq-poc.digital_attribution_modelling.bws_mta_customer_journeys_YYYYMMDD`
    Outputs checks to `wx-bq-poc.digital_attribution_modelling.bws_mta_checks_YYYYMMDD`
Usage    
    import customer_journeys
    customer_journeys.(fw, bqclient, bqstorageclient, use_dummy_data=False, rerun=False, use_dummy_date_extended=False)
Author
    Andrew Lau
"""
import logging

import google.auth
from google.cloud import bigquery
from google.cloud import bigquery_storage

def upload_dashboard_data(fw, bqclient, bqstorageclient):
    """
    Overview
        BQ script https://console.cloud.google.com/bigquery?sq=564585695625:81c04fbfe1544091818d347fd6b5ec79
        Runs downstream process for BWS MTA - CRN level allocation and update of dashboard

        Outputs customer journeys to `wx-bq-poc.digital_attribution_modelling.bws_mta_crn_output_YYYYMMDD`
        Outputs checks to `wx-bq-poc.digital_attribution_modelling.bws_mta_checks_YYYYMMDD`
    
    Arguments
        fw (string) - eg '2021-04-19'
        bqclient - google.cloud.bigquery()
        bqstorageclientgoogle.cloud.bigquery_storage()
        use_dummy_data (boolean) - generate random inc sales for testing purposes. Default - False        
    """
    logging.info("running upload_dashboard_data for " + fw)
    fw_no_dash = fw[:4] + fw[5:7] + fw[-2:]

    query_string = """
    ------------------------------------------------------
    -- UPLOAD TO DASHBOARD
    ------------------------------------------------------
    delete from `wx-bq-poc.personal.AL_MTA_dashboard`
    where banner='bws' and fw = '{fw}';

    insert into `wx-bq-poc.personal.AL_MTA_dashboard` (
        SELECT
            b.fm_start_date,
            concat('{fw}', '_', a.campaign_code) as key,
            '{fw}' as fw,
            'bws' as banner,
            a.campaign_code,
                case
                    when a.channel like "%_AlwaysOn_display%" then "AlwaysOn_display"
                    when a.channel like "%_AlwaysOn_google_other%" then "AlwaysOn_google_other"
                    when a.channel like "%_AlwaysOn_sem%" then "AlwaysOn_sem"
                    when a.channel like "%_AlwaysOn_video%" then "AlwaysOn_video"
                    when a.channel like "%_AlwaysOn_wow_web%" then "AlwaysOn_wow_web"

                    when a.channel like "%FB%" then "FB"
                    when a.channel like "%IG%" then "IG"
                    when a.channel like "%display%" then "display"
                    when a.channel like "%email%" then "email"
                    when a.channel like "%rw_app%" then "rw_app"
                    when a.channel like "%rw_web_oap%" then "rw_web_oap"
                    when a.channel like "%wow_web%" then "wow_web"

                    -- catch all to identidy ungrouped channels
                    else concat("UNGROUPED: ", a.channel)
                end as channel,
            -1 as event_volume,
            count(distinct crn) as event_reach,
            -1 as event_converted_crn,
            -1 as medium_reach,
            -1 as medium_converted_crn,
            sum(a.attributed_total_sales) as attributed_total_sales,
            sum(a.attributed_inc_sales) as attributed_inc_sales,
            -1 as attributed_inc_sales_percent
        FROM `wx-bq-poc.digital_attribution_modelling.bws_mta_crn_output_{fw_no_dash}` as a
        left join (
            select
                distinct
                    fw_start_date,
                    -- fw_end_date,
                    fp_start_date as fm_start_date,
                    -- fp_end_date as fm_end_date
            from `wx-bq-poc.loyalty.dim_date_hist`
        ) as b
        on cast(b.fw_start_date as string) = '{fw}'
        where a.campaign_code != "ONLINE"
        group by 1,2,3,4,5,6
        order by 1,2,3,4,5,7
    );

    -- return checks
    SELECT
    fw,
    count(*) as count,
    count(distinct campaign_code) as count_distinct_campaign_codes,
    sum(attributed_inc_sales) as attributed_inc_sales
    FROM
    `wx-bq-poc.personal.AL_MTA_dashboard`
    where banner='bws'
    group by 1
    order by 1
    ;

    """.format(fw=fw,
    fw_no_dash=fw_no_dash)

    df_bq = (
    bqclient.query(query_string)
    .result()
    .to_dataframe(bqstorage_client=bqstorageclient)
    )

    logging.info("ran upload_dashboard_data for " + fw)
    logging.info("dashboard updated: `wx-bq-poc.personal.AL_MTA_dashboard`")
    logging.info("dashboard updated: https://datastudio.google.com/reporting/47182796-a5f5-4401-afe0-c31d5dc6d2b6/page/PEWWC")

    # log checks
    logging.info(str(df_bq))
    
    return df_bq

if __name__ == "__main__":
    ##################################################
    # LOGGING
    ##################################################
    # logging.basicConfig(level=logging.DEBUG)
    logFormatter = logging.Formatter("%(asctime)s [%(levelname)-7.7s]  %(message)s")
    rootLogger = logging.getLogger()
    rootLogger.setLevel(logging.INFO)  # need to set root logger to the lowest level
    
    # grab credentials from default login, use gcloud auth login
    credentials, your_project_id = google.auth.default(
        scopes=["https://www.googleapis.com/auth/cloud-platform"]
    )
    
    # Make clients.
    bqclient = bigquery.Client(credentials=credentials, project='wx-bq-poc',)
    bqstorageclient = bigquery_storage.BigQueryReadClient(credentials=credentials)

    upload_dashboard_data(fw='2020-09-21',bqclient=bqclient,bqstorageclient=bqstorageclient)
   
