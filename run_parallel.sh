#!bin/bash
# sudo apt install parallel first
# usage
# bash run_parallel.sh FW_START FW_END NUM_THREADS

# run entire CY2020 with 32 threads
# bash run_parallel.sh 2020-01-06 2020-12-28 32

# run entire CY2021 with 32 threads
# bash run_parallel.sh 2021-01-04 2021-08-30 32

# run FY20/21 with 32 threads
# bash run_parallel.sh 2020-06-29 2021-06-28 16

# run ALL
# bash run_parallel.sh 2020-01-06 2021-08-30 32


# bash run_parallel.sh 2020-06-29 2021-06-28 2>&1 | tee "log.txt"

# creating new tmp file to generate loop
echo "" > tmp.txt
for (( i=0; i<$3; i++ ))
do
   echo -n "$i " >> tmp.txt
done

my_var=$(cat tmp.txt)
rm tmp.txt

source activate bws-mta

# running BWS MTA in parallel
# positional arguments:
#   start                 FW start date. Set to "latest" for latest FW.

# optional arguments:
#   -h, --help            show this help message and exit
#   -e END, --end END     FW end date
#   -t THREADS, --threads THREADS
#                         total number of threads
#   -w WORKER, --worker WORKER
#                         worker number
#   -c, --customer_journey_rerun
#                         whether to rerun customer journey creation. Default is
#                         False.
#   -d, --downstream_process_rerun
#                         whether to rerun downstream process. Default is False.
#   -m, --mta_rerun       whether to rerun MTA. Default is False.

parallel --jobs $3 -u "python main.py $1 -e $2 -c -d -m -t $3 -w" ::: $my_var

