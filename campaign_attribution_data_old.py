"""


"""
import os
import logging
import datetime

import google.auth
from google.cloud import bigquery
from google.cloud import bigquery_storage

import email_notifications
import utils


def generate_activation_opens_data(fw,
                                    BQ_project="wx-bq-poc",
                                    input_data="wx-bq-poc.loyalty_bi_analytics__prod_v3.safarievents_final_",
                                    BQ_table="digital_attribution_modelling",
                                    bqclient=None,
                                    bqstorageclient=None,
                                    rerun=False):
    logging.info("running generate_activation_opens_data for " + fw)
    fw_no_dash = fw[:4] + fw[5:7] + fw[-2:]
    # check if already ran
    if utils.check_if_BQ_table_exists("{BQ_project}.{BQ_table}.bws_activations_opens_{fw_no_dash}".format(BQ_project=BQ_project, BQ_table=BQ_table, fw_no_dash=fw_no_dash)):
        if rerun:
            logging.warning("rerunning generate_activation_opens_data")
        else:
            logging.warning("skipping generate_activation_opens_data")
            return
    query_string = """
    -- Overview:
    -- BWS
    -- Generate dataset for MC POC inc sales. BTL activations and TTL email opens.
    -- Input:
    -- `{input_data}{fw_no_dash}`
    -- Output:
    -- `{BQ_project}.{BQ_table}.bws_activations_opens_{fw_no_dash}`

    ----------------------------------------------------------------------
    -- TTL OPENS
    ----------------------------------------------------------------------
    create or replace table `wx-bq-poc.personal.AL_MCPOC_TTL_email_opens_BWS_{fw_no_dash}` as (
    with tmp as (
        select
        distinct cast(crn as STRING) as crn,
        campaign_code,
        campaign_start_date,
        campaign_end_date,
        fw_start_date,
        open_datetime,
        -- take max(start of fw, open time) as per Shen's request
        (case when cast(fw_start_date as timestamp) > open_datetime then cast(fw_start_date as timestamp) else open_datetime end) as ts
        from
        `{input_data}{fw_no_dash}`
        where
        fw_start_date = '{fw}' and
    --  BWS/Supers
        division_name = 'BWS' and
        cast(crn as STRING) != '0' and
        open_datetime is not null and
        campaign_ttl is true
    )
    select
        *,
            -- recalculate campaign week number, as Wilson's table dups for multiple week campaigns
        cast((case
                -- odd cases when activation time is before campaign start date. +1 to be inclusive of first date.
                when ceil((date_diff(cast(ts as date), campaign_start_date, DAY) + 1)/ 7) <= 0 then 1
                -- capping this to control odd behaviour, shouldn't really be campaigns lasting this long
                when ceil((date_diff(cast(ts as date), campaign_start_date, DAY) + 1)/ 7) >= 26 then 26
                else ceil((date_diff(cast(ts as date), campaign_start_date, DAY) + 1)/ 7)
                end    
        ) as INT64) as campaign_week_nbr
    from tmp
    )
    ;
    ----------------------------------------------------------------------
    -- BTL ACTIVATIONS (SAFARI DEFINITION)
    ----------------------------------------------------------------------
    -- Confirmed with Wilson the timestamps in safarievents_final are SYDNEY time, despite being labelled as UTC

    create or replace table `wx-bq-poc.personal.AL_MCPOC_BTL_activations_BWS_{fw_no_dash}` as (
    with tmp as (
        select
        distinct cast(crn as STRING) as crn,
        campaign_code,
        campaign_start_date,
        campaign_end_date,  
        fw_start_date,
        activation_datetime,
        -- take max(start of fw, activation time)
        (case when cast(fw_start_date as timestamp) > activation_datetime then cast(fw_start_date as timestamp) else activation_datetime end) as ts
        from
        `{input_data}{fw_no_dash}`
        where
        fw_start_date = '{fw}' and
    --  BWS/Supers
        division_name = 'BWS' and
        cast(crn as STRING) != '0' and
        activation_datetime is not null and
        campaign_ttl is false)
    select
        *,
        -- recalculate campaign week number, as Wilson's table dups for multiple week campaigns
        cast((case
                -- odd cases when activation time is before campaign start date. +1 to be inclusive of first date.
                when ceil((date_diff(cast(ts as date), campaign_start_date, DAY) + 1)/ 7) <= 0 then 1
                -- capping this to control odd behaviour, shouldn't really be campaigns lasting this long
                when ceil((date_diff(cast(ts as date), campaign_start_date, DAY) + 1)/ 7) >= 26 then 26
                else ceil((date_diff(cast(ts as date), campaign_start_date, DAY) + 1)/ 7)
                end    
        ) as INT64) as campaign_week_nbr
    from tmp
    )
    ;

    ----------------------------------------------------------------------
    -- UNION OF TTL EMAIL OPENS AND BTL ACTIVATIONS
    ----------------------------------------------------------------------
    create or replace table `{BQ_project}.{BQ_table}.bws_activations_opens_{fw_no_dash}` as (
    with tmp as (
    select 
        fw_start_date,
        crn,
        campaign_code,
        campaign_start_date,
        campaign_end_date,
        campaign_week_nbr,
        ts,
    --     concat(campaign_code, "_", campaign_start_date, "_wk", campaign_week_nbr) as event,
        concat(campaign_code, "_wk", campaign_week_nbr) as event,
        1 as TTL_flag
    from
        `wx-bq-poc.personal.AL_MCPOC_TTL_email_opens_BWS_{fw_no_dash}`

    union all

    select 
        fw_start_date,
        crn,
        campaign_code,
        campaign_start_date,
        campaign_end_date,
        campaign_week_nbr,
        ts,
    --     concat(campaign_code, "_", campaign_start_date, "_wk", campaign_week_nbr) as event,
        concat(campaign_code, "_wk", campaign_week_nbr) as event,
        0 as TTL_flag
    from
        `wx-bq-poc.personal.AL_MCPOC_BTL_activations_BWS_{fw_no_dash}`
    )
    select
        a.*,
        case when b.crn is null then 0 else 1 end as redemption_flag
    from tmp as a
    -- join on redemption flag
    left join (
        select
            distinct
            cast(crn as STRING) as crn,
            campaign_code,
            campaign_start_date,
            fw_start_date,
        from 
            `{input_data}{fw_no_dash}`
        where
            fw_start_date = '{fw}' and
            division_name = 'BWS' and
            cast(crn as STRING) != '0' and
            redemption_flag_campaign_week is true
    ) as b
        on a.crn = b.crn
        and a.campaign_code = b.campaign_code
        and a.campaign_start_date = b.campaign_start_date
        and a.fw_start_date = b.fw_start_date
    );

    ----------------------------------------------------------------------
    -- CHECKS
    ----------------------------------------------------------------------
    -- check TTL flag only has 1 value per campaign code
    ASSERT
    (
        with tmp as (
            select distinct campaign_code, TTL_flag from `{BQ_project}.{BQ_table}.bws_activations_opens_{fw_no_dash}`
        ), tmp2 as (
            select
                campaign_code,
                count(*) as count
            from tmp group by 1
        )
        select count(*) from tmp2 where count !=1
    ) = 0
    AS 'ERROR: inconsistent TTL flags';

    -- check that each date is mapped to only 1 campaign week number
    ASSERT
    (
        (with tmp as (
            select 
                campaign_code,
                campaign_start_date,
                campaign_week_nbr,
                cast(ts as date) as ts_date,
                count(*) as count
            from 
                `{BQ_project}.{BQ_table}.bws_activations_opens_{fw_no_dash}`
            group by 1,2,3,4
            order by 1,2,3,4
        )
        ,tmp2 as (
            select
                campaign_code,
                campaign_start_date,
                campaign_week_nbr,
                ts_date,
                count(*) as count
            from tmp
            group by 1,2,3,4

        )
        select count(*) from tmp2 where count != 1) = 0
    ) AS 'ERROR: inconsistent campaign week number';

    -- check consistent FW
    ASSERT
    (
    WITH
    tmp AS (
    SELECT
        DISTINCT fw_start_date,
        campaign_code
    FROM
        `{BQ_project}.{BQ_table}.bws_activations_opens_{fw_no_dash}`
    WHERE
        fw_start_date != '{fw}'
    ORDER BY
        1, 2)
    SELECT
        COUNT(*)
    FROM
        tmp
    ) = 0
    AS 'ERROR: invalid FWs';

    """.format(fw=fw, fw_no_dash=fw_no_dash, BQ_project=BQ_project, BQ_table=BQ_table, input_data=input_data)
    logging.debug(str(query_string))
    df_bq = (
    bqclient.query(query_string)
    .result()
    .to_dataframe(bqstorage_client=bqstorageclient)
    )

    logging.info("ran generate_activation_opens_data for " + fw)
    logging.info("`{BQ_project}.{BQ_table}.bws_activation_opens_{fw_no_dash}`".format(BQ_project=BQ_project, BQ_table=BQ_table, fw_no_dash=fw_no_dash))

    return df_bq

def generate_targeted_sent_data(fw,
                                    BQ_project="wx-bq-poc",
                                    input_data="wx-bq-poc.loyalty_bi_analytics__prod_v3.safarievents_final_",
                                    BQ_table="digital_attribution_modelling",
                                    bqclient=None,
                                    bqstorageclient=None,
                                    rerun=False):
    logging.info("running generate_targeted_sent_data for " + fw)
    fw_no_dash = fw[:4] + fw[5:7] + fw[-2:]
    # check if already ran
    if utils.check_if_BQ_table_exists("{BQ_project}.{BQ_table}.bws_targeted_sent_{fw_no_dash}".format(BQ_project=BQ_project, BQ_table=BQ_table, fw_no_dash=fw_no_dash)):
        if rerun:
            logging.warning("rerunning generate_targeted_sent_data")
        else:
            logging.warning("skipping generate_targeted_sent_data")
            return
    query_string = """
    -- Overview:
    -- BWS
    -- Generate dataset for MC POC inc sales. BTL targeted and TTL email sends.
    -- Input:
    -- `{input_data}{fw_no_dash}`
    -- Output:
    -- `{BQ_project}.{BQ_table}.bws_targeted_sent_{fw_no_dash}`

    ----------------------------------------------------------------------
    -- TTL SENT
    ----------------------------------------------------------------------
    create or replace table `wx-bq-poc.personal.AL_MCPOC_TTL_email_sends_BWS_{fw_no_dash}` as (
    with tmp as (
    select
        distinct cast(crn as STRING) as crn,
        campaign_code,
        campaign_start_date,
        campaign_end_date,
        fw_start_date,
        sent_datetime,
        -- take max(start of fw, open time)
        (case when cast(fw_start_date as timestamp) > sent_datetime then cast(fw_start_date as timestamp) else sent_datetime end) as ts
    from
        `{input_data}{fw_no_dash}`
    where
        fw_start_date = '{fw}' and
    --  BWS/Supers
        division_name = 'BWS' and
        cast(crn as STRING) != '0' and
        sent_datetime is not null and
        campaign_ttl is true
    )
    select
    *,
    -- recalculate campaign week number, as Wilson's table dups for multiple week campaigns
    cast((case
            -- odd cases when activation time is before campaign start date. +1 to be inclusive of first date.
            when ceil((date_diff(cast(ts as date), campaign_start_date, DAY) + 1)/ 7) <= 0 then 1
            -- capping this to control odd behaviour, shouldn't really be campaigns lasting this long
            when ceil((date_diff(cast(ts as date), campaign_start_date, DAY) + 1)/ 7) >= 26 then 26
            else ceil((date_diff(cast(ts as date), campaign_start_date, DAY) + 1)/ 7)
        end    
    ) as INT64) as campaign_week_nbr
    from tmp
    )
    ;

    ----------------------------------------------------------------------
    -- BTL TARGETED
    ----------------------------------------------------------------------
    -- Confirmed with Wilson the timestamps in safarievents_final are SYDNEY time, despite being labelled as UTC

    create or replace table `wx-bq-poc.personal.AL_MCPOC_BTL_targeted_BWS_{fw_no_dash}` as (
    with tmp as (
    select
        distinct cast(crn as STRING) as crn,
        campaign_code,
        campaign_start_date,
        campaign_end_date,
        fw_start_date,
        sent_datetime,
        -- take max(start of fw, sent_datetime time). if sent_datetime is null, then take start of fw
        (case
        when sent_datetime is null then cast(fw_start_date as timestamp)
        when cast(fw_start_date as timestamp) > sent_datetime then cast(fw_start_date as timestamp)
        else sent_datetime
        end) as ts
    from
        `{input_data}{fw_no_dash}`
    where
        fw_start_date = '{fw}' and
        -- BWS/SUPERS
        division_name = 'BWS' and
        cast(crn as STRING) != '0' and
        -- Wilson has said that everyone in this table is targeted, if there is a missing sent date time it's a data error
        -- sent_datetime is not null and
        campaign_ttl is false
    )
    select
    *,
    -- recalculate campaign week number, as Wilson's table dups for multiple week campaigns
    cast((case
            -- odd cases when activation time is before campaign start date. +1 to be inclusive of first date.
            when ceil((date_diff(cast(ts as date), campaign_start_date, DAY) + 1)/ 7) <= 0 then 1
            -- capping this to control odd behaviour, shouldn't really be campaigns lasting this long
            when ceil((date_diff(cast(ts as date), campaign_start_date, DAY) + 1)/ 7) >= 26 then 26
            else ceil((date_diff(cast(ts as date), campaign_start_date, DAY) + 1)/ 7)
        end    
    ) as INT64) as campaign_week_nbr
    from tmp
    )
    ;

    ----------------------------------------------------------------------
    -- UNION OF TTL EMAIL OPENS AND BTL ACTIVATIONS
    ----------------------------------------------------------------------
    create or replace table `{BQ_project}.{BQ_table}.bws_targeted_sent_{fw_no_dash}` as (
    with tmp as (
    select 
        fw_start_date,
        crn,
        campaign_code,
        campaign_start_date,
        campaign_end_date,
        campaign_week_nbr,
        ts,
    --     concat(campaign_code, "_", campaign_start_date, "_wk", campaign_week_nbr) as event,
        concat(campaign_code, "_wk", campaign_week_nbr) as event,
        1 as TTL_flag
    from
        `wx-bq-poc.personal.AL_MCPOC_TTL_email_sends_BWS_{fw_no_dash}`

    union all

    select 
        fw_start_date,
        crn,
        campaign_code,
        campaign_start_date,
        campaign_end_date,
        campaign_week_nbr,
        ts,
    --     concat(campaign_code, "_", campaign_start_date, "_wk", campaign_week_nbr) as event,
        concat(campaign_code, "_wk", campaign_week_nbr) as event,
        0 as TTL_flag
    from
        `wx-bq-poc.personal.AL_MCPOC_BTL_targeted_BWS_{fw_no_dash}`
    )
    select
        a.*,
        case when b.crn is null then 0 else 1 end as redemption_flag
    from tmp as a
    -- join on redemption flag
    left join (
        select
            distinct
            cast(crn as STRING) as crn,
            campaign_code,
            campaign_start_date,
            fw_start_date,
        from 
            `{input_data}{fw_no_dash}`
        where
            fw_start_date = '{fw}' and
            division_name = 'BWS' and
            cast(crn as STRING) != '0' and
            redemption_flag_campaign_week is true
    ) as b
        on a.crn = b.crn
        and a.campaign_code = b.campaign_code
        and a.campaign_start_date = b.campaign_start_date
        and a.fw_start_date = b.fw_start_date
    );


    ----------------------------------------------------------------------
    -- CHECKS
    ----------------------------------------------------------------------
    -- check TTL flag only has 1 value per campaign code
    ASSERT
    (
        with tmp as (
            select distinct campaign_code, TTL_flag from `{BQ_project}.{BQ_table}.bws_targeted_sent_{fw_no_dash}`
        ), tmp2 as (
            select
                campaign_code,
                count(*) as count
            from tmp group by 1
        )
        select count(*) from tmp2 where count !=1
    ) = 0
    AS 'ERROR: inconsistent TTL flags';

    -- check that each date is mapped to only 1 campaign week number
    ASSERT
    (
        (with tmp as (
            select 
                campaign_code,
                campaign_start_date,
                campaign_week_nbr,
                cast(ts as date) as ts_date,
                count(*) as count
            from 
                `{BQ_project}.{BQ_table}.bws_targeted_sent_{fw_no_dash}`
            group by 1,2,3,4
            order by 1,2,3,4
        )
        ,tmp2 as (
            select
                campaign_code,
                campaign_start_date,
                campaign_week_nbr,
                ts_date,
                count(*) as count
            from tmp
            group by 1,2,3,4

        )
        select count(*) from tmp2 where count != 1) = 0
    ) AS 'ERROR: inconsistent campaign week number';

    -- check consistent FW
    ASSERT
    (
    WITH
    tmp AS (
    SELECT
        DISTINCT fw_start_date,
        campaign_code
    FROM
        `{BQ_project}.{BQ_table}.bws_targeted_sent_{fw_no_dash}`
    WHERE
        fw_start_date != '{fw}'
    ORDER BY
        1, 2)
    SELECT
        COUNT(*)
    FROM
        tmp
    ) = 0
    AS 'ERROR: invalid FWs';

    """.format(fw=fw, fw_no_dash=fw_no_dash, BQ_project=BQ_project, BQ_table=BQ_table, input_data=input_data)
    logging.debug(str(query_string))
    df_bq = (
    bqclient.query(query_string)
    .result()
    .to_dataframe(bqstorage_client=bqstorageclient)
    )

    logging.info("ran generate_activation_opens_data for " + fw)
    logging.info("`{BQ_project}.{BQ_table}.bws_targeted_sent_{fw_no_dash}`".format(BQ_project=BQ_project, BQ_table=BQ_table, fw_no_dash=fw_no_dash))

    return df_bq
if __name__ == "__main__":
    ##################################################
    # LOGGING
    ##################################################
    # logging.basicConfig(level=logging.DEBUG)
    logFormatter = logging.Formatter("%(asctime)s [%(levelname)-7.7s]  %(message)s")
    rootLogger = logging.getLogger()
    rootLogger.setLevel(logging.INFO)  # need to set root logger to the lowest level
    
    # now = datetime.datetime.now() + datetime.timedelta(hours=10)  # convert to Sydney time
    # now = now.strftime("%d-%m-%Y_%H%M")
    # fileHandler = logging.FileHandler("{0}/{1}.log".format("logs", now), 'w+')
    # fileHandler.setLevel(logging.DEBUG)
    # fileHandler.setFormatter(logFormatter)
    # rootLogger.addHandler(fileHandler)

    # consoleHandler = logging.StreamHandler()
    # consoleHandler.setLevel(logging.INFO)
    # consoleHandler.setFormatter(logFormatter)
    # rootLogger.addHandler(consoleHandler)

    # catch all exceptions in the log
    # def my_handler(type, value, tb):
    #     rootLogger.exception("Uncaught exception: {0}".format(str(value)))

    # Install exception handler
    # sys.excepthook = my_handler

    # set user credentials again
    # os.environ['GOOGLE_APPLICATION_CREDENTIALS']="adc.json"
    

    def generate_campaign_attribution_data(fw_start,
                                            fw_end,
                                            BQ_project="wx-bq-poc",
                                            BQ_table="digital_attribution_modelling",
                                            input_data="wx-bq-poc.loyalty_bi_analytics__prod_v3.safarievents_final_",
                                            rerun=False,
                                            FW_MAX=100,
                                            bqclient=None,
                                            bqstorageclient=None):
        # set user credentials to create BQ client object
        os.environ['GOOGLE_APPLICATION_CREDENTIALS']="adc.json" 
        os.environ['GOOGLE_CLOUD_PROJECT']=BQ_project
        logging.info("os.environ['GOOGLE_CLOUD_PROJECT'] = " + str(os.environ['GOOGLE_CLOUD_PROJECT']))
        logging.info("using input_data = " + str(input_data))

        # grab credentials from default login, use gcloud auth login
        credentials, your_project_id = google.auth.default(
            scopes=["https://www.googleapis.com/auth/cloud-platform"]
        )
        
        # Make clients.
        bqclient = bigquery.Client(credentials=credentials, project=BQ_project,)
        bqstorageclient = bigquery_storage.BigQueryReadClient(credentials=credentials)

        datetime_object = datetime.datetime.strptime(fw_start, '%Y-%m-%d')

        for weeks_to_add in range(FW_MAX):        
            datetime_object_plus_weeks = datetime_object + datetime.timedelta(weeks=weeks_to_add)
            # break when reached last fw to parse
            if datetime_object_plus_weeks > datetime.datetime.strptime(fw_end, '%Y-%m-%d'):
                logging.info("breaking loop as fw_end reached")
                break
            fw = datetime_object_plus_weeks.strftime("%Y-%m-%d")
            fw_no_dash = fw[:4] + fw[5:7] + fw[-2:]
            
            # generate_activation_opens_data(fw, input_data=input_data, rerun=rerun, BQ_project=BQ_project, BQ_table=BQ_table, bqclient=bqclient, bqstorageclient=bqstorageclient)
            # generate_targeted_sent_data(fw, input_data=input_data, rerun=rerun, BQ_project=BQ_project, BQ_table=BQ_table, bqclient=bqclient, bqstorageclient=bqstorageclient)

            generate_camp_event_data(fw, input_data=input_data, rerun=rerun, BQ_project=BQ_project, BQ_table=BQ_table, bqclient=bqclient, bqstorageclient=bqstorageclient)

            # check subset consistency
            query_string = """
            -- strict subset check
            create or replace table `{BQ_project}.{BQ_table}.bws_check_subset_{fw_no_dash}` as (
            with tmp as (
                select
                -- fw_start_date, crn, campaign_start_date
                coalesce(a.fw_start_date, b.fw_start_date) as fw_start_date,
                coalesce(a.crn, b.crn) as crn,
                coalesce(a.TTL_flag, b.TTL_flag) as TTL_flag,
                coalesce(a.campaign_code, b.campaign_code) as campaign_code,

                coalesce(a.campaign_start_date, b.campaign_start_date) as campaign_start_date,
                case
                    when a.crn is not null and b.crn is not null then 'in_a_in_b'
                    when a.crn is not null and b.crn is null then 'in_a_not_b'
                    when a.crn is null and b.crn is not null then 'not_a_in_b'
                    when a.crn is null and b.crn is null then 'not_a_not_b'
                end as check,
                case
                    when a.crn is not null and b.crn is not null then 1 else 0
                end as in_a_in_b,    
                case
                    when a.crn is not null and b.crn is null then 1 else 0
                end as in_a_not_b,    
                case
                    when a.crn is null and b.crn is not null then 1 else 0
                end as not_a_in_b,    

                from
                `wx-bq-poc.digital_attribution_modelling.bws_targeted_sent_{fw_no_dash}` as a
                full outer join 
                `wx-bq-poc.digital_attribution_modelling.bws_activations_opens_{fw_no_dash}` as b
                    on a.crn = b.crn and a.campaign_code = b.campaign_code and a.campaign_start_date = b.campaign_start_date
            )
            select
                fw_start_date,
                campaign_code,
                TTL_flag,
                campaign_start_date,
                -- lots of campaigns have no activation timestamps, despite being BTL
                sum(in_a_in_b) as in_a_in_b,
                sum(in_a_not_b) as in_a_not_b,
                sum(not_a_in_b) as not_a_in_b,
            from tmp
            group by
                fw_start_date,
                campaign_code,
                TTL_flag,
                campaign_start_date
            );

            -- check consistent FW
            ASSERT
            (
            SELECT
                count(*)
            FROM
                `{BQ_project}.{BQ_table}.bws_check_subset_{fw_no_dash}`
            WHERE not_a_in_b != 0
            ) = 0
            AS 'ERROR: invalid FWs';
            """.format(fw=fw, fw_no_dash=fw_no_dash, BQ_project=BQ_project, BQ_table=BQ_table, input_data=input_data)
            logging.debug(str(query_string))
            _ = (
            bqclient.query(query_string)
            .result()
            .to_dataframe(bqstorage_client=bqstorageclient)
            )

            logging.info("ran subset consistency checks " + fw)
            logging.info("`{BQ_project}.{BQ_table}.bws_activation_opens_{fw_no_dash}`".format(BQ_project=BQ_project, BQ_table=BQ_table, fw_no_dash=fw_no_dash))

    # BQ_PROJECT = "gcp-wow-rwds-ai-safari-prod"
    # BQ_TABLE = "safari_campaign_attribution"

    # BQ_PROJECT = "wx-bq-poc"
    # BQ_TABLE = "digital_attribution_modelling"

    BQ_PROJECT_MAPPING = {"gcp-wow-rwds-ai-safari-prod":"safari_campaign_attribution"
        # , "wx-bq-poc":"digital_attribution_modelling"
        }

    for BQ_PROJECT in BQ_PROJECT_MAPPING:
        BQ_TABLE = BQ_PROJECT_MAPPING[BQ_PROJECT]

        logging.info("BQ_PROJECT = " + str(BQ_PROJECT))
        logging.info("BQ_TABLE = " + str(BQ_TABLE))

        # all weeks, use BWS Safari events data
        generate_campaign_attribution_data("2020-01-06", "2021-08-30", 
                                          input_data="wx-bq-poc.loyalty_bi_analytics__prod_v3.safarievents_final_bws_",
                                          BQ_project=BQ_PROJECT, BQ_table=BQ_TABLE, rerun=True)

        # generate_campaign_attribution_data("2021-09-06", "2021-09-13",
        #                                     input_data="wx-bq-poc.loyalty_bi_analytics__prod_v3.safarievents_final_",
        #                                     BQ_project=BQ_PROJECT, BQ_table=BQ_TABLE, rerun=True)

        email_notifications.send_email_notification(email_recipient='alau3@woolworths.com.au', subject="BWS MTA CAMPAIGN DATA DONE", message_body="", fpath_image=None)

    
